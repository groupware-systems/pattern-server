<?php
/* Copyright (C) 2014-2020  Stephan Kreutzer
 *
 * This file is part of pattern-server.
 *
 * pattern-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * pattern-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with pattern-server. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/de/index.lang.php
 * @author Stephan Kreutzer
 * @since 2014-05-31
 */



define("LANG_PAGETITLE", "Willkommen!");
define("LANG_HEADER", "Willkommen!");
define("LANG_INSTALLBUTTON", "Installieren");
define("LANG_INSTALLDELETEFAILED", "Die Installation wurde bereits erfolgreich durchgeführt, jedoch ist es der Installation nicht gelungen, sich selbst zu löschen. Bitte löschen Sie mindestens die Datei <tt>\$/install/install.php</tt> oder gleich das ganze Verzeichnis <tt>\$/install/</tt> manuell.");
define("LANG_WELCOMETEXT", "Willkommen beim <xml:lang=\"en\">Pattern Server</span>!");
define("LANG_LINKCAPTION_CREATETEMPLATE", "Vorlage anlegen");
define("LANG_LINKCAPTION_CREATEPATTERN", "Muster anlegen");
define("LANG_LINKCAPTION_BROWSEPATTERNS", "Muster durchsuchen");
define("LANG_LICENSE", "Lizenzierung");



?>
