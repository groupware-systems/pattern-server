<?php
/* Copyright (C) 2020-2022  Stephan Kreutzer
 *
 * This file is part of pattern-server.
 *
 * pattern-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * pattern-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with pattern-server. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/de/template_create.lang.php
 * @author Stephan Kreutzer
 * @since 2020-08-27
 */



define("LANG_PAGETITLE", "Neue Vorlage");
define("LANG_HEADER", "Neue Vorlage");
define("LANG_FORMLABEL_PATTERNTITLE", "Titel");
define("LANG_FORMLABEL_PATTERNNAME", "Interner technischer Name");
define("LANG_FORMLABEL_PATTERNNAMESPACE", "<span xml:lang=\"en\">Namespace</span> (<span xml:lang=\"en\">URI</span>)");
define("LANG_FORMLABEL_PATTERNSECTIONTITLE", "Titel Sektion");
define("LANG_FORMLABEL_PATTERNSECTIONNAME", "Interner technischer Name der Sektion");
define("LANG_FORMLABEL_PATTERNSECTIONRANGEMINIMUM", "minimum");
define("LANG_FORMLABEL_PATTERNSECTIONRANGEMAXIMUM", "maximum");
define("LANG_FORMLABEL_PATTERNSECTIONRANGESTEP", "Schrittwert");
define("LANG_FORMLABEL_PATTERNSECTIONRANGESTART", "Start");
define("LANG_FORMLABEL_PATTERNSECTIONLISTNAMEITEM", "Interner technischer Name der Listen-Einträge");
define("LANG_FORMLABEL_PATTERNSECTIONTEXTSTATIC", "Statischer Text");
define("LANG_FORMLABEL_SUBMIT", "anlegen");
define("LANG_OPTIONLABEL_SECTIONTYPE_TEXTEDIT", "Texteingabe");
define("LANG_OPTIONLABEL_SECTIONTYPE_RANGE", "Schieberegler");
define("LANG_OPTIONLABEL_SECTIONTYPE_LIST", "Liste");
define("LANG_OPTIONLABEL_SECTIONTYPE_TEXTSTATIC", "statischer Text");
define("LANG_LINKCAPTION_MAINPAGE", "Hauptseite");


?>
