<?php
/* Copyright (C) 2017-2022 Stephan Kreutzer
 *
 * This file is part of pattern-server.
 *
 * pattern-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * pattern-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with pattern-server. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/libraries/session.inc.php
 * @author Stephan Kreutzer
 * @since 2020-03-21
 */



if (empty($_SESSION) === true)
{
    if (@session_start() !== true)
    {
        http_response_code(403);
        exit(-1);
    }
}

if (isset($_SESSION['user_id']) !== true)
{
    http_response_code(403);
    exit(1);
}

if (isset($_SESSION['user_role']) !== true)
{
    http_response_code(403);
    exit(-1);
}

if (isset($_SESSION['instance_path']) !== true)
{
    http_response_code(403);
    exit(-1);
}
else
{
    $lhs = str_replace("\\", "/", dirname(__FILE__));
    $rhs = str_replace("\\", "/", $_SESSION['instance_path'])."/libraries";

    if ($lhs !== $rhs)
    {
        http_response_code(403);
        exit(1);
    }
}



?>
