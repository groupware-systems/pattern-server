/* Copyright (C) 2019-2023 Stephan Kreutzer
 *
 * This file is part of pattern-server.
 *
 * pattern-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * pattern-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with pattern-server. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function loadTextControl(idPattern, idSection)
{
    let updateControlDiv = document.getElementById("update_control_" + idSection);
    let revisionTextSpan = document.getElementById("revision_text_" + idSection);

    if (updateControlDiv == null)
    {
        throw "ID 'update_control_" + idSection + "' not found.";
    }

    if (revisionTextSpan == null)
    {
        throw "ID 'revision_text_" + idSection + "' not found.";
    }

    unloadControl("update_control_" + idSection);

    {
        let form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "pattern_view.php?id=" + idPattern + "&id_section=" + idSection);

        let fieldset = document.createElement("fieldset");

        let textarea = document.createElement("textarea");
        textarea.setAttribute("name", "value");
        textarea.setAttribute("rows", "24");
        textarea.setAttribute("cols", "80");

        {
            let textareaText = document.createTextNode(revisionTextSpan.innerText);
            textarea.appendChild(textareaText);
        }

        let submit = document.createElement("input");
        submit.setAttribute("type", "submit");
        submit.setAttribute("name", "submit");
        submit.setAttribute("value", "Update");

        let cancel = document.createElement("input");
        cancel.setAttribute("type", "button");
        cancel.setAttribute("onclick", "unloadControl(\"update_control_" + idSection + "\");");
        cancel.setAttribute("value", "Cancel");

        fieldset.appendChild(textarea);
        fieldset.appendChild(submit);
        fieldset.appendChild(cancel);

        form.appendChild(fieldset);
        updateControlDiv.appendChild(form);
    }

    return 0;
}

function loadRangeControl(idPattern, idSection, min, max, step)
{
    let updateControlDiv = document.getElementById("update_control_" + idSection);
    let revisionTextSpan = document.getElementById("revision_text_" + idSection);

    if (updateControlDiv == null)
    {
        throw "ID 'update_control_" + idSection + "' not found.";
    }

    if (revisionTextSpan == null)
    {
        throw "ID 'revision_text_" + idSection + "' not found.";
    }

    unloadControl("update_control_" + idSection);

    {
        let form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "pattern_view.php?id=" + idPattern + "&id_section=" + idSection);

        let fieldset = document.createElement("fieldset");

        let input = document.createElement("input");
        input.setAttribute("type", "range");
        input.setAttribute("name", "value");
        input.setAttribute("min", min);
        input.setAttribute("max", max);
        input.setAttribute("step", step);
        input.setAttribute("value", revisionTextSpan.innerText);
        input.setAttribute("orient", "horizontal");
        input.setAttribute("onchange", "document.getElementById('output').value=this.value;");

        let output = document.createElement("output");
        output.setAttribute("id", "output");

        {
            let outputValue = document.createTextNode(revisionTextSpan.innerText);
            output.appendChild(outputValue);
        }

        let submit = document.createElement("input");
        submit.setAttribute("type", "submit");
        submit.setAttribute("name", "submit");
        submit.setAttribute("value", "Update");

        let cancel = document.createElement("input");
        cancel.setAttribute("type", "button");
        cancel.setAttribute("onclick", "unloadControl(\"update_control_" + idSection + "\");");
        cancel.setAttribute("value", "Cancel");

        fieldset.appendChild(input);
        fieldset.appendChild(output);
        fieldset.appendChild(submit);
        fieldset.appendChild(cancel);

        form.appendChild(fieldset);
        updateControlDiv.appendChild(form);
    }

    return 0;
}

function loadListControl(idPattern, idSection)
{
    let updateControlDiv = document.getElementById("update_control_" + idSection);
    let revisionList = document.getElementById("revision_text_" + idSection);

    if (updateControlDiv == null)
    {
        throw "ID 'update_control_" + idSection + "' not found.";
    }

    if (revisionList == null)
    {
        throw "ID 'revision_text_" + idSection + "' not found.";
    }

    unloadControl("update_control_" + idSection);

    {
        let form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "pattern_view.php?id=" + idPattern + "&id_section=" + idSection);

        let fieldset = document.createElement("fieldset");

        let list = document.createElement("ul");
        list.setAttribute("id", "section-" + idSection);

        // Unfortunately have to do this here, so addListItemControl()
        // can find the ID...
        fieldset.appendChild(list);
        form.appendChild(fieldset);
        updateControlDiv.appendChild(form);

        let listItems = revisionList.getElementsByTagName("li");

        for (let i = 0, max = listItems.length; i < max; i++)
        {
            let newListItem = addListItemControl("section-" + idSection);
            let textarea = newListItem.getElementsByTagName("textarea");
            textarea[0].innerText = listItems.item(i).innerText;
        }

        let add = document.createElement("input");
        add.setAttribute("type", "button");
        add.setAttribute("value", "+");
        add.setAttribute("onclick", "addListItemControl(\"section-" + idSection + "\");");

        let br = document.createElement("br");

        let submit = document.createElement("input");
        submit.setAttribute("type", "submit");
        submit.setAttribute("name", "submit");
        submit.setAttribute("value", "Update");

        let cancel = document.createElement("input");
        cancel.setAttribute("type", "button");
        cancel.setAttribute("onclick", "unloadControl(\"update_control_" + idSection + "\");");
        cancel.setAttribute("value", "Cancel");

        fieldset.appendChild(add);
        fieldset.appendChild(br);
        fieldset.appendChild(submit);
        fieldset.appendChild(cancel);
    }

    return 0;
}

function unloadControl(idSection)
{
    let updateControlDiv = document.getElementById(idSection);

    if (updateControlDiv == null)
    {
        throw "ID '" + idSection + "' not found.";
    }

    for (let i = updateControlDiv.childNodes.length - 1; i >= 0; i--)
    {
        removeNode(updateControlDiv.childNodes[i], updateControlDiv);
    }

    return 0;
}

let listItemCounter = 0;

function addListItemControl(idSection)
{
    let target = document.getElementById(idSection);

    if (target == null)
    {
        throw "ID '" + idSection + "' not found.";
    }

    listItemCounter += 1;

    let idItem = "item-" + listItemCounter;

    let item = document.createElement("li");
    item.setAttribute("id", idSection + "-" + idItem);

    let textArea = document.createElement("textarea");
    item.appendChild(textArea);

    let buttonMoveUp = document.createElement("input");
    buttonMoveUp.setAttribute("type", "button");
    buttonMoveUp.setAttribute("value", "↑");
    buttonMoveUp.setAttribute("onclick", "moveListItemControl(\"" + idSection + "\", \"" + idItem + "\", -1);");
    buttonMoveUp.classList.add("list-item-button-up");
    item.appendChild(buttonMoveUp);

    let buttonMoveDown = document.createElement("input");
    buttonMoveDown.setAttribute("type", "button");
    buttonMoveDown.setAttribute("value", "↓");
    buttonMoveDown.setAttribute("onclick", "moveListItemControl(\"" + idSection + "\", \"" + idItem + "\", 1);");
    buttonMoveDown.classList.add("list-item-button-down");
    item.appendChild(buttonMoveDown);

    let buttonRemove = document.createElement("input");
    buttonRemove.setAttribute("type", "button");
    buttonRemove.setAttribute("value", "-");
    buttonRemove.setAttribute("onclick", "removeListItemControl(\"" + idSection + "\", \"" + idItem + "\");");
    item.appendChild(buttonRemove);

    target.appendChild(item);

    updateListItemTextareas(idSection);

    return item;
}

function removeListItemControl(idSection, idItem)
{
    let target = document.getElementById(idSection + "-" + idItem);

    if (target == null)
    {
        throw "ID '" + idSection + "' not found.";
    }

    target.parentNode.removeChild(target);

    updateListItemTextareas(idSection);

    return 0;
}

function moveListItemControl(idSection, idItem, direction)
{
    let itemSelected = document.getElementById(idSection + "-" + idItem);

    if (itemSelected == null)
    {
        throw "ID '" + idSection + "-" + idItem + "' not found.";
    }

    let itemDestination = null;

    if (direction == -1)
    {
        itemDestination = itemSelected.previousElementSibling;
    }
    else if (direction == 1)
    {
        itemDestination = itemSelected;
        itemSelected = itemSelected.nextElementSibling;
    }
    else
    {
        return -1;
    }

    if (itemDestination == null)
    {
        return 1;
    }

    if (itemDestination.nodeName.toLowerCase() != "li")
    {
        return -2;
    }

    // There's also itemSelected.before(itemDestination) for destination == -1
    // and itemSelected.after(itemDestination) for destination == 1 which
    // may be better for no requirement of shared parentNode?
    itemSelected.parentNode.insertBefore(itemSelected, itemDestination);

    updateListItemTextareas(idSection);

    return 0;
}

function updateListItemTextareas(idSection)
{
    let parent = document.getElementById(idSection);

    if (parent == null)
    {
        throw "ID '" + idSection + "' not found.";
    }

    let liCount = 0;
    let liLast = null;

    for (let i = 0, max = parent.childNodes.length; i < max; i++)
    {
        let item = parent.childNodes[i];

        if (item.nodeType != Node.ELEMENT_NODE)
        {
            continue;
        }

        if (item.nodeName.toLowerCase() != "li")
        {
            continue;
        }

        liCount += 1;

        for (let j = 0, max2 = item.childNodes.length, textareaCount = 0; j < max2; j++)
        {
            let child = item.childNodes[j];

            if (child.nodeType != Node.ELEMENT_NODE)
            {
                continue;
            }

            if (child.nodeName.toLowerCase() == "textarea")
            {
                if (textareaCount > 1)
                {
                    throw "Duplicate list item textarea found.";
                }

                child.setAttribute("name", idSection + "-item-" + i);
                textareaCount += 1;
            }
            else if (child.nodeName.toLowerCase() == "input")
            {
                if (child.classList.contains("list-item-button-up") == true)
                {
                    if (liCount == 1)
                    {
                        child.setAttribute("disabled", "disabled");
                    }
                    else
                    {
                        child.removeAttribute("disabled");
                    }
                }

                if (child.classList.contains("list-item-button-down") == true)
                {
                    child.removeAttribute("disabled");
                    liLast = child;
                }
            }
        }
    }

    if (liLast != null)
    {
        liLast.setAttribute("disabled", "disabled");
    }

    return 0;
}

function toggleRevisions(id)
{
    let revisionsDiv = document.getElementById(id);

    if (revisionsDiv == null)
    {
        throw "ID '" + id + "' not found.";
    }

    if (revisionsDiv.style.display === "block")
    {
        revisionsDiv.style.display = "none";
    }
    else
    {
        revisionsDiv.style.display = "block";
    }

    return 0;
}

// Stupid JavaScript has a cloneNode(deep), but no removeNode(deep).
function removeNode(element, parent)
{
    // TODO: Reverse delete for performance.
    while (element.hasChildNodes() == true)
    {
        removeNode(element.lastChild, element);
    }

    parent.removeChild(element);
}
