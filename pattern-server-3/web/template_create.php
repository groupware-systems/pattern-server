<?php
/* Copyright (C) 2020-2024 Stephan Kreutzer
 *
 * This file is part of pattern-server.
 *
 * pattern-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * pattern-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with pattern-server. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/template_create.php
 * @brief For creating a new pattern template.
 * @todo Deleting a section leaves the empty bordered <div/>.
 * @author Stephan Kreutzer
 * @since 2020-08-27
 */



require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");

require_once(dirname(__FILE__)."/libraries/user_management.inc.php");

if (((int)($_SESSION['user_role'])) !== USER_ROLE_ADMIN)
{
    http_response_code(403);
    exit(1);
}


require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("template_create"));



function validateXmlElementName($name)
{
    if (is_string($name) !== true)
    {
        return false;
    }

    if (strlen($name) <= 0)
    {
        return false;
    }

    /** @todo This might depend on locale. */
    if (ctype_alpha($name[0]) !== true &&
        $name[0] != '_')
    {
        return false;
    }

    if (stripos($name, "xml") === 0)
    {
        return false;
    }

    for ($i = 1, $max = strlen($name); $i < $max; $i++)
    {
        /** @todo This might depend on locale. */
        if (ctype_alnum($name[$i]) !== true &&
            $name[$i] != '-' &&
            $name[$i] != '_' &&
            $name[$i] != '.')
        {
            return false;
        }
    }

    return true;
}



$sections = array();

if (count($_POST) > 0)
{
    /** @todo There might be no guarantee of the order of the input elements
      * being retained in $_POST, but that would be important for the patterns.
      * Can some ordinal numbering be added? */

    $sectionTypeTextEdit = 0;
    $sectionTypeRange = 0;
    $sectionTypeList = 0;
    $sectionTypeTextStatic = 0;

    foreach ($_POST as $key => $value)
    {
        if (strpos($key, "section-name-") === 0)
        {
            $id = substr($key, strlen("section-name-"));

            if ($id === false)
            {
                http_response_code(500);
                return 1;
            }

            if (strlen($id) <= 0)
            {
                http_response_code(400);
                return 1;
            }

            if (isset($_POST["section-title-".$id]) !== true)
            {
                http_response_code(400);
                return 1;
            }

            if (isset($sections[$id]) === true)
            {
                http_response_code(400);
                return 1;
            }

            if (validateXmlElementName($_POST["section-name-".$id]) != true)
            {
                http_response_code(400);
                return 1;
            }

            $section = array("name" => $_POST["section-name-".$id],
                             "title" => $_POST["section-title-".$id],
                             "type" => "1");

            if (isset($_POST["section-range-minimum-".$id]) === true &&
                isset($_POST["section-range-maximum-".$id]) === true &&
                isset($_POST["section-range-step-".$id]) === true &&
                isset($_POST["section-range-start-".$id]) === true)
            {
                $minimum = (int)$_POST["section-range-minimum-".$id];
                $maximum = (int)$_POST["section-range-maximum-".$id];
                $step = (int)$_POST["section-range-step-".$id];
                $start = (int)$_POST["section-range-start-".$id];

                if ($maximum <= $minimum)
                {
                    http_response_code(400);
                    return 1;
                }

                if ($step < 1)
                {
                    http_response_code(400);
                    return 1;
                }

                if ($step > $maximum)
                {
                    http_response_code(400);
                    return 1;
                }

                if ($start < $minimum ||
                    $start > $maximum)
                {
                    http_response_code(400);
                    return 1;
                }

                if (($maximum - $minimum) < $step)
                {
                    http_response_code(400);
                    return 1;
                }

                $section["type"] = "2";
                $section["minimum"] = $minimum;
                $section["maximum"] = $maximum;
                $section["step"] = $step;
                $section["start"] = $start;

                $sectionTypeRange++;
            }
            else if (isset($_POST["section-list-item-name-".$id]) === true)
            {
                $itemName = $_POST["section-list-item-name-".$id];

                if (validateXmlElementName($itemName) != true)
                {
                    http_response_code(400);
                    return 1;
                }

                $section["type"] = "3";
                $section["item-name"] = $itemName;

                $sectionTypeList++;
            }
            else if (isset($_POST["section-text-static-".$id]) === true)
            {
                $flags = 0;

                if (isset($_POST["section-text-static-".$id."-input"]) === true)
                {
                    $flags += 1;
                }

                if (isset($_POST["section-text-static-".$id."-output"]) === true)
                {
                    $flags += 2;
                }

                if (isset($_POST["section-text-static-".$id."-data"]) === true)
                {
                    $flags += 4;
                }

                if ($flags == 0)
                {
                    http_response_code(400);
                    return 1;
                }

                $section["text-static"] = $_POST["section-text-static-".$id];
                $section["type"] = "".(3 + $flags);

                $sectionTypeTextStatic++;
            }
            else
            {
                $sectionTypeTextEdit++;
            }

            $sections[$id] = $section;
        }
    }

    if ($sectionTypeTextEdit <= 0 &&
        $sectionTypeRange <= 0 &&
        $sectionTypeList <= 0)
    {
        http_response_code(400);
        return 1;
    }
}

$inserted = false;

if (isset($_POST['name']) === true &&
    isset($_POST['title']) === true &&
    isset($_POST['namespace']) === true &&
    count($sections) > 0)
{
    if (validateXmlElementName($_POST["name"]) != true)
    {
        http_response_code(400);
        return 1;
    }

    require_once("./libraries/database.inc.php");

    if (Database::Get()->IsConnected() !== true)
    {
        http_response_code(500);
        return 1;
    }

    $idTemplate = -1;
    $success = Database::Get()->BeginTransaction();

    if ($success === true)
    {
        $idTemplate = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."template` (`id`,\n".
                                              "    `namespace`,\n".
                                              "    `name`,\n".
                                              "    `title`,\n".
                                              "    `datetime_created`)\n".
                                              "VALUES (?, ?, ?, ?, UTC_TIMESTAMP())\n",
                                              array(NULL, $_POST['namespace'], $_POST['name'], $_POST['title']),
                                              array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_STRING, Database::TYPE_STRING));

        if ($idTemplate <= 0)
        {
            $success = false;
        }
    }

    /** @todo Check if it maybe would be more performant to add all values
      * to a single INSERT, but the downside is preparing the PDO arrays. */

    foreach ($sections as $section)
    {
        $idSection = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."template_section` (`id`,\n".
                                              "    `name`,\n".
                                              "    `title`,\n".
                                              "    `type`,\n".
                                              "    `id_template`)\n".
                                              "VALUES (?, ?, ?, ?, ?)\n",
                                              array(NULL, $section['name'], $section['title'], $section['type'], $idTemplate),
                                              array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_STRING, Database::TYPE_INT, Database::TYPE_INT));

        if ($idSection <= 0)
        {
            $success = false;
            break;
        }

        if (((int)$section["type"]) == 2)
        {
            $idRange = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."template_section_range` (`id`,\n".
                                                  "    `minimum`,\n".
                                                  "    `maximum`,\n".
                                                  "    `step`,\n".
                                                  "    `start`,\n".
                                                  "    `id_template`,\n".
                                                  "    `id_template_section`)\n".
                                                  "VALUES (?, ?, ?, ?, ?, ?, ?)\n",
                                                  array(NULL, $section['minimum'], $section['maximum'], $section['step'], $section['start'], $idTemplate, $idSection),
                                                  array(Database::TYPE_NULL, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT, Database::TYPE_INT));

            if ($idRange <= 0)
            {
                $success = false;
                break;
            }
        }
        else if (((int)$section["type"]) == 3)
        {
            $idListItem = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."template_section_list` (`id`,\n".
                                                  "    `item_name`,\n".
                                                  "    `id_template`,\n".
                                                  "    `id_template_section`)\n".
                                                  "VALUES (?, ?, ?, ?)\n",
                                                  array(NULL, $section['item-name'], $idTemplate, $idSection),
                                                  array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_INT, Database::TYPE_INT));

            if ($idListItem <= 0)
            {
                $success = false;
                break;
            }
        }
        else if (((int)$section["type"]) >= 4 &&
                 ((int)$section["type"]) <= 10)
        {
            $idTextStatic = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."template_section_textstatic` (`id`,\n".
                                                    "    `text`,\n".
                                                    "    `datetime_created`,\n".
                                                    "    `id_template`,\n".
                                                    "    `id_template_section`)\n".
                                                    "VALUES (?, ?, UTC_TIMESTAMP(), ?, ?)\n",
                                                    array(NULL, $section["text-static"], $idTemplate, $idSection),
                                                    array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_INT, Database::TYPE_INT));

            if ($idTextStatic <= 0)
            {
                $success = false;
                break;
            }
        }
    }

    if ($success === true)
    {
        if (Database::Get()->CommitTransaction() === true)
        {
            $inserted = true;
        }
        else
        {
            http_response_code(500);
            return 1;
        }
    }
    else
    {
        Database::Get()->RollbackTransaction();
        http_response_code(500);
        return 1;
    }
}


if ($inserted !== true)
{
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
         "<!DOCTYPE html\n".
         "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
         "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
         "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
         "  <head>\n".
         "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
         "    <title>".LANG_PAGETITLE."</title>\n".
         "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
         "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
         "    <style type=\"text/css\">\n".
         "      .container-section\n".
         "      {\n".
         "          border: 1px solid black;\n".
         "      }\n".
         "\n".
         "      .validation:invalid\n".
         "      {\n".
         "          border: #FF0000 solid 3px;\n".
         "      }\n".
         "    </style>\n".
         "    <script type=\"text/javascript\">\n".
         "      \"use strict\";\n".
         "\n".
         "      let sectionCount = 0;\n".
         "\n".
         "      function addSection()\n".
         "      {\n".
         "          let sectionType = document.getElementById(\"section-type\");\n".
         "\n".
         "          if (sectionType == null)\n".
         "          {\n".
         "              console.log(\"Was unable to find element with the ID \\\"section-type\\\".\");\n".
         "              return -1;\n".
         "          }\n".
         "          else\n".
         "          {\n".
         "              sectionType = sectionType.value;\n".
         "          }\n".
         "\n".
         "          let target = document.getElementById(\"sections\");\n".
         "\n".
         "          if (target == null)\n".
         "          {\n".
         "              console.log(\"Was unable to find element with the ID \\\"sections\\\".\");\n".
         "              return -1;\n".
         "          }\n".
         "\n".
         "          let container = document.createElement(\"div\");\n".
         "          container.setAttribute(\"id\", \"section-\" + sectionCount);\n".
         "          container.setAttribute(\"class\", \"container-section\");\n".
         "\n".
         "          let label = document.createElement(\"label\");\n".
         "          label.setAttribute(\"for\", \"section-title-\" + sectionCount);\n".
         "          label.appendChild(document.createTextNode(\"".LANG_FORMLABEL_PATTERNSECTIONTITLE."\"));\n".
         "          container.appendChild(label);\n".
         "\n".
         "          let title = document.createElement(\"input\");\n".
         "          title.setAttribute(\"type\", \"text\");\n".
         "          title.setAttribute(\"name\", \"section-title-\" + sectionCount);\n".
         "          title.setAttribute(\"id\", \"section-title-\" + sectionCount);\n".
         "          title.setAttribute(\"required\", \"true\");\n".
         "          container.appendChild(title);\n".
         "\n".
         "          label = document.createElement(\"label\");\n".
         "          label.setAttribute(\"for\", \"section-name-\" + sectionCount);\n".
         "          label.appendChild(document.createTextNode(\"".LANG_FORMLABEL_PATTERNSECTIONNAME."\"));\n".
         "          container.appendChild(label);\n".
         "          let name = document.createElement(\"input\");\n".
         "          name.setAttribute(\"type\", \"text\");\n".
         "          name.setAttribute(\"name\", \"section-name-\" + sectionCount);\n".
         "          name.setAttribute(\"id\", \"section-name-\" + sectionCount);\n".
         // Pattern isn't perfect, but useful enough. The server may still refuse it.
         "          name.setAttribute(\"pattern\", \"[a-zA-Z]{1}[a-zA-Z0-9\\\\-]{0,254}\");\n".
         "          name.setAttribute(\"required\", \"true\");\n".
         "          name.classList.add(\"validation\");\n".
         "          container.appendChild(name);\n".
         "\n".
         "          let remove = document.createElement(\"input\");\n".
         "          remove.setAttribute(\"type\", \"button\");\n".
         "          remove.setAttribute(\"value\", \"-\");\n".
         "          remove.setAttribute(\"onclick\", \"removeSection(\\\"section-\" + sectionCount + \"\\\");\");\n".
         "          container.appendChild(remove);\n".
         "\n".
         "          if (sectionType == \"text-edit\")\n".
         "          {\n".
         "\n".
         "          }\n".
         "          else if (sectionType == \"range\")\n".
         "          {\n".
         "              let br = document.createElement(\"br\");\n".
         "              container.appendChild(br);\n".
         "\n".
         "              label = document.createElement(\"label\");\n".
         "              label.setAttribute(\"for\", \"section-range-minimum-\" + sectionCount);\n".
         "              label.appendChild(document.createTextNode(\"".LANG_FORMLABEL_PATTERNSECTIONRANGEMINIMUM."\"));\n".
         "              container.appendChild(label);\n".
         "              name = document.createElement(\"input\");\n".
         "              name.setAttribute(\"type\", \"number\");\n".
         "              name.setAttribute(\"name\", \"section-range-minimum-\" + sectionCount);\n".
         "              name.setAttribute(\"id\", \"section-range-minimum-\" + sectionCount);\n".
         "              name.setAttribute(\"value\", \"1\");\n".
         "              name.setAttribute(\"required\", \"true\");\n".
         "              container.appendChild(name);\n".
         "\n".
         "              label = document.createElement(\"label\");\n".
         "              label.setAttribute(\"for\", \"section-range-maximum-\" + sectionCount);\n".
         "              label.appendChild(document.createTextNode(\"".LANG_FORMLABEL_PATTERNSECTIONRANGEMAXIMUM."\"));\n".
         "              container.appendChild(label);\n".
         /** @todo Better variable names. */
         "              name = document.createElement(\"input\");\n".
         "              name.setAttribute(\"type\", \"number\");\n".
         "              name.setAttribute(\"name\", \"section-range-maximum-\" + sectionCount);\n".
         "              name.setAttribute(\"id\", \"section-range-maximum-\" + sectionCount);\n".
         "              name.setAttribute(\"value\", \"2\");\n".
         "              name.setAttribute(\"required\", \"true\");\n".
         "              container.appendChild(name);\n".
         "\n".
         "              label = document.createElement(\"label\");\n".
         "              label.setAttribute(\"for\", \"section-range-step-\" + sectionCount);\n".
         "              label.appendChild(document.createTextNode(\"".LANG_FORMLABEL_PATTERNSECTIONRANGESTEP."\"));\n".
         "              container.appendChild(label);\n".
         "              name = document.createElement(\"input\");\n".
         "              name.setAttribute(\"type\", \"number\");\n".
         "              name.setAttribute(\"name\", \"section-range-step-\" + sectionCount);\n".
         "              name.setAttribute(\"id\", \"section-range-step-\" + sectionCount);\n".
         "              name.setAttribute(\"value\", \"1\");\n".
         "              name.setAttribute(\"min\", \"1\");\n".
         "              container.appendChild(name);\n".
         "\n".
         "              label = document.createElement(\"label\");\n".
         "              label.setAttribute(\"for\", \"section-range-start-\" + sectionCount);\n".
         "              label.appendChild(document.createTextNode(\"".LANG_FORMLABEL_PATTERNSECTIONRANGESTART."\"));\n".
         "              container.appendChild(label);\n".
         "              name = document.createElement(\"input\");\n".
         "              name.setAttribute(\"type\", \"number\");\n".
         "              name.setAttribute(\"name\", \"section-range-start-\" + sectionCount);\n".
         "              name.setAttribute(\"id\", \"section-range-start-\" + sectionCount);\n".
         "              name.setAttribute(\"value\", \"1\");\n".
         "              container.appendChild(name);\n".
         "          }\n".
         "          else if (sectionType == \"list\")\n".
         "          {\n".
         "              let br = document.createElement(\"br\");\n".
         "              container.appendChild(br);\n".
         "\n".
         "              label = document.createElement(\"label\");\n".
         "              label.setAttribute(\"for\", \"section-list-item-name-\" + sectionCount);\n".
         "              label.appendChild(document.createTextNode(\"".LANG_FORMLABEL_PATTERNSECTIONLISTNAMEITEM."\"));\n".
         "              container.appendChild(label);\n".
         "              name = document.createElement(\"input\");\n".
         "              name.setAttribute(\"type\", \"text\");\n".
         "              name.setAttribute(\"name\", \"section-list-item-name-\" + sectionCount);\n".
         "              name.setAttribute(\"id\", \"section-list-item-name-\" + sectionCount);\n".
         // Pattern isn't perfect, but useful enough. The server may still refuse it.
         "              name.setAttribute(\"pattern\", \"[a-zA-Z]{1}[a-zA-Z0-9\\\\-]{0,254}\");\n".
         "              name.setAttribute(\"required\", \"true\");\n".
         "              name.classList.add(\"validation\");\n".
         "              container.appendChild(name);\n".
         "          }\n".
         "          else if (sectionType == \"text-static\")\n".
         "          {\n".
         "              let br = document.createElement(\"br\");\n".
         "              container.appendChild(br);\n".
         "\n".
         "              label = document.createElement(\"label\");\n".
         "              label.setAttribute(\"for\", \"section-text-static-\" + sectionCount);\n".
         "              label.appendChild(document.createTextNode(\"".LANG_FORMLABEL_PATTERNSECTIONTEXTSTATIC."\"));\n".
         "              container.appendChild(label);\n".
         "              name = document.createElement(\"textarea\");\n".
         "              name.setAttribute(\"name\", \"section-text-static-\" + sectionCount);\n".
         "              name.setAttribute(\"id\", \"section-text-static-\" + sectionCount);\n".
         "              container.appendChild(name);\n".
         "\n".
         "              let checkboxInputLabel = document.createElement(\"label\");\n".
         "              checkboxInputLabel.setAttribute(\"for\", \"section-text-static-\" + sectionCount + \"-input\");\n".
         "              checkboxInputLabel.appendChild(document.createTextNode(\"".LANG_FORMLABEL_PATTERNSECTIONTEXTSTATICINPUT."\"));\n".
         "              container.appendChild(checkboxInputLabel);\n".
         "              let checkboxInput = document.createElement(\"input\");\n".
         "              checkboxInput.setAttribute(\"type\", \"checkbox\");\n".
         "              checkboxInput.setAttribute(\"id\", \"section-text-static-\" + sectionCount + \"-input\");\n".
         "              checkboxInput.setAttribute(\"name\", \"section-text-static-\" + sectionCount + \"-input\");\n".
         "              container.appendChild(checkboxInput);\n".
         "\n".
         "              let checkboxOutputLabel = document.createElement(\"label\");\n".
         "              checkboxOutputLabel.setAttribute(\"for\", \"section-text-static-\" + sectionCount + \"-output\");\n".
         "              checkboxOutputLabel.appendChild(document.createTextNode(\"".LANG_FORMLABEL_PATTERNSECTIONTEXTSTATICOUTPUT."\"));\n".
         "              container.appendChild(checkboxOutputLabel);\n".
         "              let checkboxOutput = document.createElement(\"input\");\n".
         "              checkboxOutput.setAttribute(\"type\", \"checkbox\");\n".
         "              checkboxOutput.setAttribute(\"id\", \"section-text-static-\" + sectionCount + \"-output\");\n".
         "              checkboxOutput.setAttribute(\"name\", \"section-text-static-\" + sectionCount + \"-output\");\n".
         "              container.appendChild(checkboxOutput);\n".
         "\n".
         "              let checkboxDataLabel = document.createElement(\"label\");\n".
         "              checkboxDataLabel.setAttribute(\"for\", \"section-text-static-\" + sectionCount + \"-data\");\n".
         "              checkboxDataLabel.appendChild(document.createTextNode(\"".LANG_FORMLABEL_PATTERNSECTIONTEXTSTATICDATA."\"));\n".
         "              container.appendChild(checkboxDataLabel);\n".
         "              let checkboxData = document.createElement(\"input\");\n".
         "              checkboxData.setAttribute(\"type\", \"checkbox\");\n".
         "              checkboxData.setAttribute(\"id\", \"section-text-static-\" + sectionCount + \"-data\");\n".
         "              checkboxData.setAttribute(\"name\", \"section-text-static-\" + sectionCount + \"-data\");\n".
         "              container.appendChild(checkboxData);\n".
         "          }\n".
         "          else\n".
         "          {\n".
         "              console.log(\"Unknown section type \\\"\" + sectionType + \"\\\".\");\n".
         "              return -1;\n".
         "          }\n".
         "\n".
         "          target.appendChild(container);\n".
         "          sectionCount += 1;\n".
         "\n".
         "          return 0;\n".
         "      }\n".
         "\n".
         "      function removeSection(id)\n".
         "      {\n".
         "          let target = document.getElementById(id);\n".
         "\n".
         "          if (target == null)\n".
         "          {\n".
         "              return 1;\n".
         "          }\n".
         "\n".
         "          /*\n".
         "          for (let i = target.childNodes.length - 1; i >= 0; i--)\n".
         "          {\n".
         "              removeNode(target.childNodes[i], target);\n".
         "          }\n".
         "          */\n".
         "\n".
         "          removeNode(target, target.parentNode);\n".
         "\n".
         "          return 0;\n".
         "      }\n".
         "\n".
         "      // Stupid JavaScript has a cloneNode(deep), but no removeNode(deep).\n".
         "      function removeNode(element, parent)\n".
         "      {\n".
         "          // TODO: Reverse delete for performance.\n".
         "          while (element.hasChildNodes() == true)\n".
         "          {\n".
         "              removeNode(element.lastChild, element);\n".
         "          }\n".
         "\n".
         "          parent.removeChild(element);\n".
         "      }\n".
         "\n".
         "      window.onload = function() {\n".
         //"          addSection();\n".
         "      };\n".
         "    </script>\n".
         "  </head>\n".
         "  <body>\n".
         "    <div class=\"mainbox\">\n".
         "      <div class=\"mainbox_header\">\n".
         "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
         "      </div>\n".
         "      <div class=\"mainbox_body\">\n".
         "        <form method=\"post\" action=\"template_create.php\">\n".
         "          <fieldset>\n".
         "            <label for=\"title\">".LANG_FORMLABEL_PATTERNTITLE."</label>\n".
         "            <input type=\"text\" name=\"title\" id=\"title\" required=\"true\"/>\n".
         "            <label for=\"name\">".LANG_FORMLABEL_PATTERNNAME."</label>\n".
         // Pattern isn't perfect, but useful enough. The server may still refuse it.
         "            <input type=\"text\" name=\"name\" id=\"name\" pattern=\"[a-zA-Z]{1}[a-zA-Z0-9\\-]{0,254}\" required=\"true\" class=\"validation\"/>\n".
         "            <label for=\"namespace\">".LANG_FORMLABEL_PATTERNNAMESPACE."</label>\n".
         "            <input type=\"text\" name=\"namespace\" id=\"namespace\" required=\"true\"/>\n".
         "            <div id=\"sections\"></div>\n".
         "            <select id=\"section-type\" size=\"1\" autocomplete=\"off\">\n".
         "              <option value=\"text-edit\">".LANG_OPTIONLABEL_SECTIONTYPE_TEXTEDIT."</option>\n".
         "              <option value=\"range\">".LANG_OPTIONLABEL_SECTIONTYPE_RANGE."</option>\n".
         "              <option value=\"list\">".LANG_OPTIONLABEL_SECTIONTYPE_LIST."</option>\n".
         "              <option value=\"text-static\">".LANG_OPTIONLABEL_SECTIONTYPE_TEXTSTATIC."</option>\n".
         "            </select>\n".
         "            <input type=\"button\" value=\"+\" onclick=\"addSection();\"/><br/>\n".
         "            <input type=\"submit\" value=\"".LANG_FORMLABEL_SUBMIT."\"/>\n".
         "          </fieldset>\n".
         "        </form>\n".
         "        <div>\n".
         "          <a href=\"index.php\">".LANG_LINKCAPTION_MAINPAGE."</a>\n".
         "        </div>\n".
         "      </div>\n".
         "    </div>\n".
         "  </body>\n".
         "</html>\n".
         "\n";
}
else
{
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
         "<!DOCTYPE html\n".
         "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
         "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
         "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
         "  <head>\n".
         "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
         "    <title>".LANG_PAGETITLE."</title>\n".
         "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
         "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
         "  </head>\n".
         "  <body>\n".
         "    <div class=\"mainbox\">\n".
         "      <div class=\"mainbox_header\">\n".
         "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
         "      </div>\n".
         "      <div class=\"mainbox_body\">\n".
         "        <a href=\"index.php\">".LANG_LINKCAPTION_MAINPAGE."</a>\n".
         "      </div>\n".
         "    </div>\n".
         "  </body>\n".
         "</html>\n".
         "\n";
}


?>
