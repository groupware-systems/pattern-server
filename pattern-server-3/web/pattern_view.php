<?php
/* Copyright (C) 2020-2023 Stephan Kreutzer
 *
 * This file is part of pattern-server.
 *
 * pattern-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * pattern-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with pattern-server. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/pattern_view.php
 * @brief View/download the pattern data.
 * @author Stephan Kreutzer
 * @since 2020-09-01
 */



require_once("./libraries/https.inc.php");

if ($_SERVER['REQUEST_METHOD'] !== "GET" &&
    $_SERVER['REQUEST_METHOD'] !== "POST")
{
    http_response_code(405);
    return 0;
}

if (isset($_GET['id']) !== true)
{
    http_response_code(400);
    return 0;
}

$id = (int)$_GET['id'];

require_once("./libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    return 1;
}

$sections = Database::Get()->Query("SELECT `".Database::Get()->GetPrefix()."pattern_section`.`id` AS `pattern_section_id`,\n".
                                   "    `".Database::Get()->GetPrefix()."pattern_section`.`id_template_section` AS `pattern_section_id_template_section`,\n".
                                   "    `".Database::Get()->GetPrefix()."template_section`.`id` AS `template_section_id`,\n".
                                   "    `".Database::Get()->GetPrefix()."template_section`.`name` AS `name`,\n".
                                   "    `".Database::Get()->GetPrefix()."template_section`.`title` AS `title`,\n".
                                   "    `".Database::Get()->GetPrefix()."template_section`.`type` AS `type`,\n".
                                   "    `".Database::Get()->GetPrefix()."template_section`.`id_template` AS `id_template`\n".
                                   "FROM `".Database::Get()->GetPrefix()."pattern_section`\n".
                                   "INNER JOIN `".Database::Get()->GetPrefix()."template_section` ON\n".
                                   "    `".Database::Get()->GetPrefix()."pattern_section`.`id_template_section`=\n".
                                   "    `".Database::Get()->GetPrefix()."template_section`.`id`\n".
                                   "WHERE `".Database::Get()->GetPrefix()."pattern_section`.`id_pattern`=?\n".
                                   "ORDER BY `".Database::Get()->GetPrefix()."pattern_section`.`id` ASC\n",
                                   array($id),
                                   array(Database::TYPE_INT));

if (is_array($sections) !== true)
{
    http_response_code(500);
    return 1;
}

$sectionsCount = count($sections);

if ($sectionsCount <= 0)
{
    http_response_code(404);
    return 0;
}

$templateId = (int)$sections[0]['id_template'];

$sectionTypeTextEdit = 0;
$sectionTypeRange = 0;
$sectionTypeList = 0;
$sectionTypeTextStatic = 0;

{
    for ($i = 0; $i < $sectionsCount; $i++)
    {
        $sections[$i]['type'] = (int)$sections[$i]['type'];

        if ($sections[$i]['type'] == 1)
        {
            $sectionTypeTextEdit++;
        }
        else if ($sections[$i]['type'] == 2)
        {
            $sectionTypeRange++;
        }
        else if ($sections[$i]['type'] == 3)
        {
            $sectionTypeList++;
        }
        else if ($sections[$i]["type"] >= 4 &&
                 $sections[$i]["type"] <= 10)
        {
            $sectionTypeTextStatic++;
        }
        else
        {
            continue;
        }
    }

    if ($sectionTypeRange > 0)
    {
        $ranges = Database::Get()->Query("SELECT `id`,\n".
                                         "    `minimum`,\n".
                                         "    `maximum`,\n".
                                         "    `step`,\n".
                                         "    `start`,\n".
                                         "    `id_template_section`\n".
                                         "FROM `".Database::Get()->GetPrefix()."template_section_range`\n".
                                         "WHERE `id_template`=?",
                                         array($templateId),
                                         array(Database::TYPE_INT));

        if (is_array($ranges) !== true)
        {
            http_response_code(500);
            return 1;
        }

        $rangeCount = count($ranges);

        if ($rangeCount <= 0)
        {
            http_response_code(500);
            return 1;
        }

        for ($i = 0; $i < $sectionsCount; $i++)
        {
            for ($j = 0; $j < $rangeCount; $j++)
            {
                if (((int)($sections[$i]['template_section_id'])) == ((int)($ranges[$j]['id_template_section'])))
                {
                    $sections[$i]['minimum'] = (int)$ranges[$j]['minimum'];
                    $sections[$i]['maximum'] = (int)$ranges[$j]['maximum'];
                    $sections[$i]['step'] = (int)$ranges[$j]['step'];
                    $sections[$i]['start'] = (int)$ranges[$j]['start'];
                }
            }
        }
    }
    else if ($sectionTypeTextStatic > 0)
    {
        $staticTexts = Database::Get()->Query("SELECT `id`,\n".
                                              "    `text`,\n".
                                              "    `datetime_created`,\n".
                                              "    `id_pattern_since`,\n".
                                              "    `id_template`,\n".
                                              "    `id_template_section`\n".
                                              "FROM `".Database::Get()->GetPrefix()."template_section_textstatic`\n".
                                              "WHERE `id_template`=?",
                                              array($templateId),
                                              array(Database::TYPE_INT));

        if (is_array($staticTexts) !== true)
        {
            http_response_code(500);
            return 1;
        }

        $staticTextsCount = count($staticTexts);

        if ($staticTextsCount <= 0)
        {
            http_response_code(500);
            return 1;
        }

        for ($i = 0; $i < $sectionsCount; $i++)
        {
            for ($j = 0; $j < $staticTextsCount; $j++)
            {
                if (((int)$sections[$i]["template_section_id"]) == ((int)$staticTexts[$j]["id_template_section"]))
                {
                    $sections[$i]["text"] = $staticTexts[$j]["text"];
                }
            }
        }
    }
}

if ($_SERVER['REQUEST_METHOD'] === "POST")
{
    if (isset($_GET['id_section']) !== true)
    {
        http_response_code(400);
        return 0;
    }

    $idSectionTarget = (int)$_GET['id_section'];
    $sectionFound = false;
    $value = "";

    for ($i = 0; $i < $sectionsCount; $i++)
    {
        if (((int)($sections[$i]['pattern_section_id'])) === $idSectionTarget)
        {
            $sectionFound = true;

            if ($sections[$i]['type'] == 1)
            {
                if (isset($_POST['value']) !== true)
                {
                    http_response_code(400);
                    return 0;
                }

                $value = $_POST['value'];
            }
            else if ($sections[$i]['type'] == 2)
            {
                if (isset($_POST['value']) !== true)
                {
                    http_response_code(400);
                    return 0;
                }

                $value = (int)$_POST['value'];

                $minimum = $sections[$i]['minimum'];
                $maximum = $sections[$i]['maximum'];
                $step = $sections[$i]['step'];
                $start = $sections[$i]['start'];

                if ($value < $minimum)
                {
                    http_response_code(400);
                    return 1;
                }

                if ($value > $maximum)
                {
                    http_response_code(400);
                    return 1;
                }

                if ((($value - $start) % $step) != 0)
                {
                    http_response_code(400);
                    return 1;
                }
            }
            else if ($sections[$i]['type'] == 3)
            {
                $value = "";

                for ($j = 0; true; $j++)
                {
                    if (isset($_POST["section-".$idSectionTarget."-item-".$j]) != true)
                    {
                        break;
                    }

                    if (strlen($value) > 0)
                    {
                        $value .= ",";
                    }

                    $value .= "\"";
                    $value .= str_replace("\"", "\"\"", $_POST["section-".$idSectionTarget."-item-".$j]);
                    $value .= "\"";
                }
            }
            else if ($sections[$i]['type'] >= 4 &&
                     $sections[$i]["type"] <= 10)
            {
                // Can't be updated.
                http_response_code(400);
                return 1;
            }
            else
            {
                http_response_code(500);
                return 1;
            }

            break;
        }
    }

    if ($sectionFound == false)
    {
        http_response_code(400);
        return 0;
    }

    $idNewRevision = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."pattern_section_revision` (`id`,\n".
                                             "    `text`,\n".
                                             "    `datetime_created`,\n".
                                             "    `id_pattern_section`)\n".
                                             "VALUES (?, ?, UTC_TIMESTAMP(), ?)",
                                             array(NULL, $value, $idSectionTarget),
                                             array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_INT));

    if ($idNewRevision <= 0)
    {
        http_response_code(500);
        exit(1);
    }
}

$revisions = Database::Get()->Query("SELECT `".Database::Get()->GetPrefix()."pattern_section`.`id` AS `pattern_section_id`,\n".
                                    "    `".Database::Get()->GetPrefix()."pattern_section`.`id_template_section` AS `pattern_section_id_template_section`,\n".
                                    "    `".Database::Get()->GetPrefix()."pattern_section_revision`.`id` AS `pattern_section_revision_id`,\n".
                                    "    `".Database::Get()->GetPrefix()."pattern_section_revision`.`text` AS `pattern_section_revision_text`,\n".
                                    "    `".Database::Get()->GetPrefix()."pattern_section_revision`.`datetime_created` AS `pattern_section_revision_datetime_created`\n".
                                    "FROM `".Database::Get()->GetPrefix()."pattern_section_revision`\n".
                                    "INNER JOIN `".Database::Get()->GetPrefix()."pattern_section` ON\n".
                                    "    `".Database::Get()->GetPrefix()."pattern_section`.`id` =\n".
                                    "    `".Database::Get()->GetPrefix()."pattern_section_revision`.`id_pattern_section`\n".
                                    "WHERE `".Database::Get()->GetPrefix()."pattern_section`.`id_pattern`=?\n".
                                    "ORDER BY `".Database::Get()->GetPrefix()."pattern_section`.`id` ASC,\n".
                                    "    `".Database::Get()->GetPrefix()."pattern_section_revision`.`datetime_created` DESC\n",
                                    array($id),
                                    array(Database::TYPE_INT));

if (is_array($revisions) !== true)
{
    http_response_code(500);
    return 1;
}

$maxRevisions = count($revisions);

if ($maxRevisions <= 0)
{
    http_response_code(404);
    return 0;
}

$template = Database::Get()->Query("SELECT `id`,\n".
                                   "    `namespace`,\n".
                                   "    `name`,\n".
                                   "    `title`\n".
                                   "FROM `".Database::Get()->GetPrefix()."template`\n".
                                   "WHERE `id`=?\n",
                                   array($templateId),
                                   array(Database::TYPE_INT));

if (is_array($template) !== true)
{
    http_response_code(500);
    return 1;
}

if (count($template) <= 0)
{
    http_response_code(500);
    return 1;
}

$template = $template[0];


require_once("./libraries/negotiation.inc.php");

NegotiateContentType(array(CONTENT_TYPE_SUPPORTED_XHTML,
                           CONTENT_TYPE_SUPPORTED_XML));

if (CONTENT_TYPE_REQUESTED === CONTENT_TYPE_SUPPORTED_XHTML)
{
    // If the name isn't a valid XML tag name and contains special XML characters,
    // escaping them is deliberately supposed to break the format, as such a situation
    // should be prevented on capturing the input.
    $templateName = htmlspecialchars($template['name'], ENT_XHTML, "UTF-8");
    $templateTitle = htmlspecialchars($template['title'], ENT_XHTML, "UTF-8");
    $templateNamespace = htmlspecialchars($template['namespace'], ENT_XHTML | ENT_QUOTES, "UTF-8");

    require_once("./libraries/languagelib.inc.php");
    require_once(getLanguageFile("pattern_view"));

    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
         "<!DOCTYPE html\n".
         "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
         "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
         "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\">\n".
         "  <head>\n".
         "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
         "    <title>".$templateTitle."</title>\n".
         "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
         "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
         "    <style type=\"text/css\">\n".
         "      .revisions\n".
         "      {\n".
         "          display: none;\n".
         "      }\n".
         "    </style>\n".
         "    <script type=\"text/javascript\" src=\"pattern_section_controls.js\"></script>\n".
         "  </head>\n".
         "  <body>\n".
         "    <div class=\"mainbox\">\n".
         "      <div class=\"mainbox_header\">\n".
         "        <h1 class=\"mainbox_header_h1\">".$templateTitle."</h1>\n".
         "      </div>\n".
         "      <div class=\"mainbox_body\">\n".
         "        <div>\n";

    for ($i = 0; $i < $sectionsCount; $i++)
    {
        if ($templateId != ((int)$sections[$i]['id_template']))
        {
            echo "<!-- Different templates associated with the sections of a pattern! -->\n";
            return 1;
        }

        if ($sections[$i]["type"] >= 4 &&
            $sections[$i]["type"] <= 10)
        {
            $flags = ((int)$sections[$i]["type"]) - 3;

            if (($flags & 2) == 2)
            {
                echo "          <div>\n".
                     "            <h2>".htmlspecialchars($sections[$i]["title"], ENT_XHTML | ENT_QUOTES, "UTF-8")."</h2>\n".
                     "            <p>\n".
                     "              ".htmlspecialchars($sections[$i]["text"], ENT_XHTML | ENT_QUOTES, "UTF-8")."\n".
                     "            </p>\n".
                     "          </div>\n";
            }
        }
        else
        {
            echo "          <div>\n";

            $firstFound = false;

            /** @todo Optimize: $revisions is already ordered, $j doesn't need to restart from 0. */
            for ($j = 0; $j < $maxRevisions; $j++)
            {
                if (((int)$revisions[$j]['pattern_section_id_template_section']) == ((int)$sections[$i]['pattern_section_id_template_section']))
                {
                    $revisionText = "";

                    if ($sections[$i]['type'] === 1 ||
                        $sections[$i]['type'] === 2)
                    {
                        $revisionText = htmlspecialchars($revisions[$j]['pattern_section_revision_text'], ENT_XHTML, "UTF-8");
                    }
                    else if ($sections[$i]['type'] === 3)
                    {
                        $revisionText = "<ul>";

                        $listItems = extractCsv($revisions[$j]['pattern_section_revision_text']);

                        if (is_array($listItems) === true)
                        {
                            if (count($listItems) <= 0)
                            {
                                $revisionText = "";
                            }
                            else
                            {
                                for ($itemIndex = 0, $itemCount = count($listItems); $itemIndex < $itemCount; $itemIndex++)
                                {
                                    $revisionText .= "<li>".htmlspecialchars($listItems[$itemIndex], ENT_XHTML, "UTF-8")."</li>";
                                }
                            }
                        }

                        $revisionText .= "</ul>";
                    }

                    if ($firstFound == false)
                    {
                        $sectionTitle = htmlspecialchars($sections[$i]['title'], ENT_XHTML, "UTF-8");

                        echo "            <h2>".$sectionTitle."</h2>\n".
                             "            <div id=\"update_control_".$sections[$i]['pattern_section_id']."\"></div>\n";

                        if ($sections[$i]['type'] === 1 ||
                            $sections[$i]['type'] === 2)
                        {
                            echo "            <p id=\"revision_text_".$sections[$i]['pattern_section_id']."\">".$revisionText."</p>\n";
                        }
                        else if ($sections[$i]['type'] === 3)
                        {
                            echo "            <div id=\"revision_text_".$sections[$i]['pattern_section_id']."\">".$revisionText."</div>\n";
                        }
                        else
                        {
                            echo "            <!-- Unsupported section type. -->\n";
                        }

                        echo "            ";

                        if ($sections[$i]['type'] === 1)
                        {
                            echo "<a href=\"#\" onclick=\"loadTextControl(".$id.", ".$sections[$i]['pattern_section_id'].");\">".LANG_LINKCAPTION_EDIT."</a> ";
                        }
                        else if ($sections[$i]['type'] === 2)
                        {
                            echo "<a href=\"#\" onclick=\"loadRangeControl(".$id.", ".$sections[$i]['pattern_section_id'].", ".$sections[$i]['minimum'].", ".$sections[$i]['maximum'].", ".$sections[$i]['step'].");\">".LANG_LINKCAPTION_EDIT."</a> ";
                        }
                        else if ($sections[$i]['type'] == 3)
                        {
                            echo "<a href=\"#\" onclick=\"loadListControl(".$id.", ".$sections[$i]['pattern_section_id'].");\">".LANG_LINKCAPTION_EDIT."</a> ";
                        }

                        echo "<a href=\"#\" onclick=\"toggleRevisions('revisions_".$sections[$i]['pattern_section_id']."');\">".LANG_LINKCAPTION_REVISIONS."</a>\n";

                        echo "            <div class=\"revisions\" id=\"revisions_".$sections[$i]['pattern_section_id']."\">\n".
                             "              <table border=\"1\">\n".
                             "                <thead>\n".
                             "                  <tr>\n".
                             "                    <th>".LANG_TABLECOLUMNCAPTION_TIMESTAMP." (UTC)</th>\n".
                             "                    <th>".LANG_TABLECOLUMNCAPTION_VERSION."</th>\n".
                             "                  </tr>\n".
                             "                </thead>\n".
                             "                <tbody>\n";

                        $firstFound = true;
                    }

                    echo "                  <tr>\n".
                         "                    <td>".$revisions[$j]['pattern_section_revision_datetime_created']."Z</td>\n".
                         "                    <td>".$revisionText."</td>\n".
                         "                  </tr>\n";
                }
            }

            if ($firstFound == true)
            {
                echo "                </tbody>\n".
                     "              </table>\n".
                     "            </div>\n";
            }

            echo "          </div>\n";
        }
    }

    echo "        </div>\n".
         "        <div>\n".
         "          (<a href=\"pattern_view.php?id=".$id."&amp;format=xml\">".LANG_LINKCAPTION_VIEWSOURCE."</a>)\n".
         "        </div>\n".
         "        <div>\n".
         "          <a href=\"pattern_select.php?id_template=".$templateId."\">".LANG_LINKCAPTION_PATTERNINDEX."</a>\n".
         "        </div>\n".
         "      </div>\n".
         "    </div>\n".
         "  </body>\n".
         "</html>\n".
         "\n";
}
else if (CONTENT_TYPE_REQUESTED === CONTENT_TYPE_SUPPORTED_XML)
{
    $listItemNames = null;

    if ($sectionTypeList > 0)
    {
        $listItemNames = Database::Get()->Query("SELECT `id`,\n".
                                                "    `item_name`,\n".
                                                "    `id_template_section`\n".
                                                "FROM `".Database::Get()->GetPrefix()."template_section_list`\n".
                                                "WHERE `id_template`=?",
                                                array($templateId),
                                                array(Database::TYPE_INT));

        if (is_array($listItemNames) !== true)
        {
            http_response_code(500);
            return 1;
        }

        $listItemNamesCount = count($listItemNames);

        if ($listItemNamesCount <= 0)
        {
            http_response_code(500);
            return 1;
        }

        $listItemNamesPrepared = array();

        for ($i = 0; $i < $listItemNamesCount; $i++)
        {
            if (array_key_exists(((int)$listItemNames[$i]["id_template_section"]), $listItemNamesPrepared) == true)
            {
                http_response_code(500);
                return 1;
            }

            // If the name isn't a valid XML tag name and contains special XML characters,
            // escaping them is deliberately supposed to break the format, as such a situation
            // should be prevented on capturing the input.
            $listItemName = htmlspecialchars($listItemNames[$i]["item_name"], ENT_XML1, "UTF-8");

            $listItemNamesPrepared[((int)$listItemNames[$i]["id_template_section"])] = $listItemName;
        }

        $listItemNames = $listItemNamesPrepared;
    }

    // If the name isn't a valid XML tag name and contains special XML characters,
    // escaping them is deliberately supposed to break the format, as such a situation
    // should be prevented on capturing the input.
    $templateName = htmlspecialchars($template['name'], ENT_XML1, "UTF-8");
    $templateNamespace = htmlspecialchars($template['namespace'], ENT_XML1 | ENT_QUOTES, "UTF-8");

    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
         "<".$templateName." xmlns=\"".$templateNamespace."\">";

    for ($i = 0; $i < $sectionsCount; $i++)
    {
        if ($templateId != ((int)$sections[$i]['id_template']))
        {
            echo "<!-- Different templates associated with the sections of a pattern! -->\n";
            return 1;
        }

        // If the name isn't a valid XML tag name and contains special XML characters,
        // escaping them is deliberately supposed to break the format, as such a situation
        // should be prevented on capturing the input.
        $sectionName = htmlspecialchars($sections[$i]['name'], ENT_XML1, "UTF-8");

        if ($sections[$i]["type"] >= 4 &&
            $sections[$i]["type"] <= 10)
        {
            $flags = ((int)$sections[$i]["type"]) - 3;

            if (($flags & 4) == 4)
            {
                echo "<".$sectionName.">".
                     htmlspecialchars($sections[$i]["text"], ENT_XML1, "UTF-8").
                     "</".$sectionName.">";
            }
        }
        else
        {
            echo "<".$sectionName.">";

            /** @todo Optimize: $revisions is already ordered, $j doesn't need to restart from 0. */
            for ($j = 0; $j < $maxRevisions; $j++)
            {
                if (((int)$revisions[$j]['pattern_section_id_template_section']) == ((int)$sections[$i]['pattern_section_id_template_section']))
                {
                    if ($sections[$i]['type'] == 3)
                    {
                        $values = extractCsv($revisions[$j]['pattern_section_revision_text']);

                        if (is_array($values) !== true)
                        {
                            continue;
                        }

                        if (array_key_exists(((int)$sections[$i]["pattern_section_id_template_section"]), $listItemNames) !== true)
                        {
                            http_response_code(500);
                            return 1;
                        }

                        $listItemName = $listItemNames[((int)$sections[$i]["pattern_section_id_template_section"])];

                        for ($valueIndex = 0, $valueMax = count($values); $valueIndex < $valueMax; $valueIndex++)
                        {
                            echo "<".$listItemName.">".
                                 htmlspecialchars($values[$valueIndex], ENT_XML1, "UTF-8").
                                 "</".$listItemName.">";
                        }
                    }
                    else
                    {
                        echo htmlspecialchars($revisions[$j]['pattern_section_revision_text'], ENT_XML1, "UTF-8");
                    }

                    break;
                }
            }

            echo "</".$sectionName.">";
        }
    }

    echo "</".$templateName.">\n";
}
else
{
    http_response_code(501);
    return 1;
}


function extractCsv($input)
{
    $max = strlen($input);

    if ($max <= 0)
    {
        return null;
    }

    $values = array();
    $i = 0;

    do
    {
        if ($input[$i] !== "\"")
        {
            return null;
        }

        $i += 1;

        $value = "";

        for ($j = $i; true; $j++)
        {
            if ($j >= $max)
            {
                return null;
            }

            if ($input[$j] === "\"")
            {
                if (($j + 1) >= $max)
                {
                    $values[] = $value;
                    return $values;
                }

                if ($input[$j + 1] === ",")
                {
                    $values[] = $value;
                    $i = $j + 2;
                    break;
                }
                else if ($input[$j + 1] === "\"")
                {
                    $value .= "\"";
                    $j += 1;
                }
            }
            else
            {
                $value .= $input[$j];
            }
        }

    } while (true);
}



?>
