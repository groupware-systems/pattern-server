<?php
/* Copyright (C) 2020-2023 Stephan Kreutzer
 *
 * This file is part of pattern-server.
 *
 * pattern-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * pattern-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with pattern-server. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/de/pattern_create.lang.php
 * @author Stephan Kreutzer
 * @since 2020-08-27
 */



define("LANG_PAGETITLE", "Muster erstellen");
define("LANG_HEADER", "Muster erstellen");
define("LANG_FORMLABEL_SUBMIT", "veröffentlichen");
define("LANG_LINKCAPTION_VIEWPATTERN", "Muster betrachten");
define("LANG_LINKCAPTION_CREATEPATTERN", "Muster anlegen");
define("LANG_LINKCAPTION_MAINPAGE", "Hauptseite");


?>
