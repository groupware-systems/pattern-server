<?php
/* Copyright (C) 2014-2022  Stephan Kreutzer
 *
 * This file is part of pattern-server.
 *
 * pattern-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * pattern-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with pattern-server. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/en/index.lang.php
 * @author Stephan Kreutzer
 * @since 2014-05-31
 */



define("LANG_PAGETITLE", "Welcome!");
define("LANG_HEADER", "Welcome!");
define("LANG_INSTALLBUTTON", "Install");
define("LANG_INSTALLDELETEFAILED", "The installation was already completed successfully, but it was unable to delete itself. Please delete at least the file <tt>\$/install/install.php</tt> or additionally the entire directory <tt>\$/install/</tt> manually.");
define("LANG_WELCOMETEXT", "Welcome to the pattern server!");
define("LANG_LINKCAPTION_CREATETEMPLATE", "Create template");
define("LANG_LINKCAPTION_CREATEPATTERN", "Create pattern");
define("LANG_LINKCAPTION_BROWSEPATTERNS", "Browse patterns");
define("LANG_LOGINDESCRIPTION", "Login for administrators:");
define("LANG_NAMEFIELD_CAPTION", "Name");
define("LANG_PASSWORDFIELD_CAPTION", "Password");
define("LANG_SUBMITBUTTON", "OK");
define("LANG_DBCONNECTFAILED", "Can’t connect to database.");
define("LANG_LOGINSUCCESS", "Login was successful!");
define("LANG_LOGINFAILED", "Password incorrect!");
define("LANG_LINKCAPTION_CONTINUE", "Continue");
define("LANG_BUTTON_LOGOUT", "Log out");
define("LANG_LINKCAPTION_RETRYLOGIN", "Retry");
define("LANG_LICENSE", "Licensing");



?>
