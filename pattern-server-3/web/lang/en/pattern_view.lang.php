<?php
/* Copyright (C) 2020  Stephan Kreutzer
 *
 * This file is part of pattern-server.
 *
 * pattern-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * pattern-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with pattern-server. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/en/pattern_view.lang.php
 * @author Stephan Kreutzer
 * @since 2020-10-20
 */



define("LANG_LINKCAPTION_VIEWSOURCE", "source");
define("LANG_LINKCAPTION_PATTERNINDEX", "Patterns");
define("LANG_LINKCAPTION_EDIT", "edit");
define("LANG_LINKCAPTION_REVISIONS", "Revisions");
define("LANG_TABLECOLUMNCAPTION_TIMESTAMP", "Timestamp");
define("LANG_TABLECOLUMNCAPTION_VERSION", "Version");



?>
