<?php
/* Copyright (C) 2020-2023 Stephan Kreutzer
 *
 * This file is part of pattern-server.
 *
 * pattern-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * pattern-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with pattern-server. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/pattern_create.php
 * @brief For creating a new pattern based on a template.
 * @author Stephan Kreutzer
 * @since 2020-08-27
 */



require_once("./libraries/https.inc.php");

require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("pattern_create"));

if (isset($_GET['id_template']) !== true)
{
    http_response_code(400);
    return 0;
}

if (is_numeric($_GET['id_template']) !== true)
{
    http_response_code(400);
    return 0;
}

$templateId = (int)$_GET['id_template'];

require_once("./libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    return 1;
}

$template = Database::Get()->Query("SELECT `title`\n".
                                   "FROM `".Database::Get()->GetPrefix()."template`\n".
                                   "WHERE `id`=?",
                                   array($templateId),
                                   array(Database::TYPE_INT));

if (is_array($template) !== true)
{
    http_response_code(500);
    return 1;
}

if (count($template) > 0)
{
    $template = $template[0];
}
else
{
    http_response_code(404);
    return 0;
}

$sections = Database::Get()->Query("SELECT `id`,\n".
                                   "    `title`,\n".
                                   "    `type`\n".
                                   "FROM `".Database::Get()->GetPrefix()."template_section`\n".
                                   "WHERE `id_template`=?",
                                   array($templateId),
                                   array(Database::TYPE_INT));

if (is_array($sections) !== true)
{
    http_response_code(500);
    return 1;
}

$sectionCount = count($sections);

{
    $sectionTypeTextEdit = 0;
    $sectionTypeRange = 0;
    $sectionTypeList = 0;
    $sectionTypeTextStatic = 0;

    for ($i = 0; $i < $sectionCount; $i++)
    {
        $sections[$i]['type'] = (int)$sections[$i]['type'];

        /** @todo Change magic number into a define included from $/libraries/. */
        if ($sections[$i]['type'] == 1)
        {
            $sectionTypeTextEdit++;
        }
        else if ($sections[$i]['type'] == 2)
        {
            $sectionTypeRange++;
        }
        else if ($sections[$i]['type'] == 3)
        {
            $sectionTypeList++;
        }
        else if ($sections[$i]["type"] >= 4 &&
                 $sections[$i]["type"] <= 10)
        {
            $sectionTypeTextStatic++;
        }
        else
        {
            http_response_code(500);
            return 1;
        }
    }

    if ($sectionTypeRange > 0)
    {
        $ranges = Database::Get()->Query("SELECT `id`,\n".
                                         "    `minimum`,\n".
                                         "    `maximum`,\n".
                                         "    `step`,\n".
                                         "    `start`,\n".
                                         "    `id_template_section`\n".
                                         "FROM `".Database::Get()->GetPrefix()."template_section_range`\n".
                                         "WHERE `id_template`=?",
                                         array($templateId),
                                         array(Database::TYPE_INT));

        if (is_array($ranges) !== true)
        {
            http_response_code(500);
            return 1;
        }

        $rangeCount = count($ranges);

        if ($rangeCount <= 0)
        {
            http_response_code(500);
            return 1;
        }

        for ($i = 0; $i < $sectionCount; $i++)
        {
            for ($j = 0; $j < $rangeCount; $j++)
            {
                if (((int)$sections[$i]['id']) == ((int)$ranges[$j]['id_template_section']))
                {
                    $sections[$i]['minimum'] = (int)$ranges[$j]['minimum'];
                    $sections[$i]['maximum'] = (int)$ranges[$j]['maximum'];
                    $sections[$i]['step'] = (int)$ranges[$j]['step'];
                    $sections[$i]['start'] = (int)$ranges[$j]['start'];
                }
            }
        }
    }

    if ($sectionTypeTextStatic > 0)
    {
        $staticTexts = Database::Get()->Query("SELECT `id`,\n".
                                              "    `text`,\n".
                                              "    `datetime_created`,\n".
                                              "    `id_pattern_since`,\n".
                                              "    `id_template`,\n".
                                              "    `id_template_section`\n".
                                              "FROM `".Database::Get()->GetPrefix()."template_section_textstatic`\n".
                                              "WHERE `id_template`=?",
                                              array($templateId),
                                              array(Database::TYPE_INT));

        if (is_array($staticTexts) !== true)
        {
            http_response_code(500);
            return 1;
        }

        $staticTextsCount = count($staticTexts);

        if ($staticTextsCount <= 0)
        {
            http_response_code(500);
            return 1;
        }

        for ($i = 0; $i < $sectionCount; $i++)
        {
            for ($j = 0; $j < $staticTextsCount; $j++)
            {
                if (((int)$sections[$i]['id']) == ((int)$staticTexts[$j]['id_template_section']))
                {
                    $sections[$i]['text'] = $staticTexts[$j]['text'];
                }
            }
        }
    }
}

$sectionValues = array();

for ($i = 0; $i < $sectionCount; $i++)
{
    if (isset($_POST['section-'.$sections[$i]['id']]) === true)
    {
        if ($sections[$i]['type'] == 2)
        {
            $value = ((int)$_POST['section-'.$sections[$i]['id']]);

            $minimum = $sections[$i]['minimum'];
            $maximum = $sections[$i]['maximum'];
            $step = $sections[$i]['step'];
            $start = $sections[$i]['start'];

            if ($value < $minimum)
            {
                http_response_code(400);
                return 1;
            }

            if ($value > $maximum)
            {
                http_response_code(400);
                return 1;
            }

            if ((($value - $start) % $step) != 0)
            {
                http_response_code(400);
                return 1;
            }

            $sectionValues[$sections[$i]['id']] = $value;
        }
        else if ($sections[$i]["type"] >= 4 &&
                 $sections[$i]["type"] <= 10)
        {
            $sectionValues[$sections[$i]['id']] = null;
        }
        else
        {
            $sectionValues[$sections[$i]['id']] = $_POST['section-'.$sections[$i]['id']];
        }
    }
    else if (isset($_POST['section-'.$sections[$i]['id']."-item-0"]) === true)
    {
        $value = "";

        for ($j = 0; true; $j++)
        {
            if (isset($_POST['section-'.$sections[$i]['id']."-item-".$j]) !== true)
            {
                break;
            }

            if (strlen($value) > 0)
            {
                $value .= ",";
            }

            $value .= "\"";
            $value .= str_replace("\"", "\"\"", $_POST['section-'.$sections[$i]['id']."-item-".$j]);
            $value .= "\"";
        }

        $sectionValues[$sections[$i]['id']] = $value;
    }
    else
    {
        $sectionValues = array();
        break;
    }
}

$inserted = false;
$idPattern = -1;

if (count($sectionValues) > 0)
{
    if (Database::Get()->IsConnected() !== true)
    {
        http_response_code(500);
        return 1;
    }

    $idPattern = -1;
    $success = Database::Get()->BeginTransaction();

    if ($success === true)
    {
        $idPattern = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."pattern` (`id`,\n".
                                             "    `datetime_created`,\n".
                                             "    `id_template`)".
                                             "VALUES (?, UTC_TIMESTAMP(), ?)\n",
                                             array(NULL, $templateId),
                                             array(Database::TYPE_NULL, Database::TYPE_INT));

        if ($idPattern <= 0)
        {
            $success = false;
        }
    }

    if ($success === true)
    {
        /** @todo Check if it maybe would be more performant to add all values
          * to a single INSERT, but the downside is preparing the PDO arrays. */

        foreach ($sectionValues as $sectionId => $sectionValue)
        {
            $idPatternSection = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."pattern_section` (`id`,\n".
                                                        "    `id_pattern`,\n".
                                                        "    `id_template_section`)\n".
                                                        "VALUES (?, ?, ?)\n",
                                                        array(NULL, $idPattern, $sectionId),
                                                        array(Database::TYPE_NULL, Database::TYPE_INT, Database::TYPE_INT));

            if ($idPatternSection <= 0)
            {
                $success = false;
                break;
            }

            if ($sectionValue === null)
            {
                // Creating the pattern_section entry but no revisions for it
                // helps the JOIN query in pattern_view to get a record and
                // join template data into it, even if the section type does
                // not have nor rely on revisions.
                continue;
            }

            $idPatternSectionRevision = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."pattern_section_revision` (`id`,\n".
                                                                "    `text`,\n".
                                                                "    `datetime_created`,\n".
                                                                "    `id_pattern_section`)\n".
                                                                // Might get the timestamp from the INSERT into pattern_section,
                                                                // or all of them from PHP.
                                                                "VALUES (?, ?, UTC_TIMESTAMP(), ?)\n",
                                                                array(NULL, $sectionValue, $idPatternSection),
                                                                array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_INT));

            if ($idPatternSectionRevision <= 0)
            {
                $success = false;
                break;
            }
        }
    }

    if ($success === true)
    {
        if (Database::Get()->CommitTransaction() === true)
        {
            $inserted = true;
        }
        else
        {
            http_response_code(500);
            return 1;
        }
    }
    else
    {
        Database::Get()->RollbackTransaction();
        http_response_code(500);
        return 1;
    }
}


if ($inserted !== true)
{
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
         "<!DOCTYPE html\n".
         "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
         "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
         "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance \" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
         "  <head>\n".
         "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
         "    <title>".LANG_PAGETITLE."</title>\n".
         "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
         "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
         "    <script type=\"text/javascript\" src=\"pattern_section_controls.js\"></script>\n".
         "  </head>\n".
         "  <body>\n".
         "    <div class=\"mainbox\">\n".
         "      <div class=\"mainbox_header\">\n".
         "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
         "      </div>\n".
         "      <div class=\"mainbox_body\">\n".
         "        <h2>".htmlspecialchars($template['title'], ENT_XHTML, "UTF-8")."</h2>\n".
         "        <form id=\"template_form\" action=\"pattern_create.php?id_template=".$templateId."\" method=\"post\">\n".
         "          <fieldset>\n";

    for ($i = 0; $i < $sectionCount; $i++)
    {
        if ($sections[$i]['type'] == 1)
        {
            echo "            <div>\n".
                 "              <div>\n".
                 "                <label for=\"section-".htmlspecialchars($sections[$i]['id'], ENT_XHTML | ENT_QUOTES, "UTF-8")."\">".htmlspecialchars($sections[$i]['title'], ENT_XHTML | ENT_QUOTES, "UTF-8")."</label>\n".
                 "              </div>\n".
                 "              <div>\n".
                 "                <textarea name=\"section-".htmlspecialchars($sections[$i]['id'], ENT_XHTML | ENT_QUOTES, "UTF-8")."\" id=\"section-".htmlspecialchars($sections[$i]['id'], ENT_XHTML | ENT_QUOTES, "UTF-8")."\"></textarea>\n".
                 "              </div>\n".
                 "            </div>\n";
        }
        else if ($sections[$i]['type'] == 2)
        {
            echo "            <div>\n".
                 "              <div>\n".
                 "                <label for=\"section-".htmlspecialchars($sections[$i]['id'], ENT_XHTML | ENT_QUOTES, "UTF-8")."\">".htmlspecialchars($sections[$i]['title'], ENT_XHTML | ENT_QUOTES, "UTF-8")."</label>\n".
                 "              </div>\n".
                 "              <div>\n".
                 "                <input type=\"range\" name=\"section-".htmlspecialchars($sections[$i]['id'], ENT_XHTML | ENT_QUOTES, "UTF-8")."\" id=\"section-".htmlspecialchars($sections[$i]['id'], ENT_XHTML | ENT_QUOTES, "UTF-8")."\" min=\"".$sections[$i]['minimum']."\" max=\"".$sections[$i]['maximum']."\" step=\"".$sections[$i]['step']."\" value=\"".$sections[$i]['start']."\" orient=\"horizontal\" onchange=\"document.getElementById('section-".htmlspecialchars($sections[$i]['id'], ENT_XHTML | ENT_QUOTES, "UTF-8")."-output').value=this.value;\"/> <output id=\"section-".htmlspecialchars($sections[$i]['id'], ENT_XHTML | ENT_QUOTES, "UTF-8")."-output\" for=\"section-".htmlspecialchars($sections[$i]['id'], ENT_XHTML | ENT_QUOTES, "UTF-8")."\">".$sections[$i]['start']."</output>\n".
                 "              </div>\n".
                 "            </div>\n";
        }
        else if ($sections[$i]['type'] == 3)
        {
            echo "            <div>\n".
                 "              <div>\n".
                 "                ".htmlspecialchars($sections[$i]['title'], ENT_XHTML | ENT_QUOTES, "UTF-8")."\n".
                 "              </div>\n".
                 "              <ul id=\"section-".htmlspecialchars($sections[$i]['id'], ENT_XHTML | ENT_QUOTES, "UTF-8")."\"></ul>\n".
                 "              <div>\n".
                 "                <input type=\"button\" value=\"+\" onclick=\"addListItemControl('section-".htmlspecialchars($sections[$i]['id'], ENT_XHTML | ENT_QUOTES, "UTF-8")."');\"/><br/>\n".
                 "              </div>\n".
                 "            </div>\n";
        }
        else if ($sections[$i]["type"] >= 4 &&
                 $sections[$i]["type"] <= 10)
        {
            echo "            <div>\n";

            $flags = ((int)$sections[$i]["type"]) - 3;

            if (($flags & 1) == 1)
            {
                echo "              <h3>".htmlspecialchars($sections[$i]['title'], ENT_XHTML | ENT_QUOTES, "UTF-8")."</h3>\n".
                     "              <p>\n".
                     "                ".htmlspecialchars($sections[$i]['text'], ENT_XHTML | ENT_QUOTES, "UTF-8")."\n".
                     "              </p>\n";
            }

            echo "              <input type=\"hidden\" name=\"section-".htmlspecialchars($sections[$i]['id'], ENT_XHTML | ENT_QUOTES, "UTF-8")."\" value=\"\"/>\n".
                 "            </div>\n";
        }
        else
        {
            echo "            <!-- Section with unknown type. -->\n";
        }
    }

    echo "            <input type=\"submit\" value=\"".LANG_FORMLABEL_SUBMIT."\"/>\n".
         "          </fieldset>\n".
         "        </form>\n".
         "        <div>\n".
         "          <a href=\"index.php\">".LANG_LINKCAPTION_MAINPAGE."</a>\n".
         "        </div>\n".
         "      </div>\n".
         "    </div>\n".
         "  </body>\n".
         "</html>\n".
         "\n";
}
else
{
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
         "<!DOCTYPE html\n".
         "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
         "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
         "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
         "  <head>\n".
         "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
         "    <title>".LANG_PAGETITLE."</title>\n".
         "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
         "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
         "  </head>\n".
         "  <body>\n".
         "    <div class=\"mainbox\">\n".
         "      <div class=\"mainbox_header\">\n".
         "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
         "      </div>\n".
         "      <div class=\"mainbox_body\">\n".
         "        <a href=\"pattern_view.php?id=".$idPattern."\">".LANG_LINKCAPTION_VIEWPATTERN."</a><br/>\n".
         "        <a href=\"pattern_create.php?id_template=".$templateId."\">".LANG_LINKCAPTION_CREATEPATTERN."</a><br/>\n".
         "        <a href=\"index.php\">".LANG_LINKCAPTION_MAINPAGE."</a>\n".
         "      </div>\n".
         "    </div>\n".
         "  </body>\n".
         "</html>\n".
         "\n";
}


?>
