<?php
/* Copyright (C) 2020  Stephan Kreutzer
 *
 * This file is part of pattern-server.
 *
 * pattern-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * pattern-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with pattern-server. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/en/template_create.lang.php
 * @author Stephan Kreutzer
 * @since 2020-08-27
 */



define("LANG_PAGETITLE", "New Template");
define("LANG_HEADER", "New Template");
define("LANG_FORMLABEL_PATTERNTITLE", "Title");
define("LANG_FORMLABEL_PATTERNNAME", "Internal Technical Name");
define("LANG_FORMLABEL_PATTERNNAMESPACE", "Namespace (URI)");
define("LANG_FORMLABEL_PATTERNSECTIONTITLE", "Title Section");
define("LANG_FORMLABEL_PATTERNSECTIONNAME", "Internal Technical Name of the Section");
define("LANG_FORMLABEL_SUBMIT", "publish");
define("LANG_LINKCAPTION_MAINPAGE", "Main page");


?>
