<?php
/* Copyright (C) 2020  Stephan Kreutzer
 *
 * This file is part of pattern-server.
 *
 * pattern-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * pattern-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with pattern-server. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/pattern_view.php
 * @brief View/download the pattern data.
 * @author Stephan Kreutzer
 * @since 2020-09-01
 */



require_once("./libraries/https.inc.php");

if ($_SERVER['REQUEST_METHOD'] !== "GET" &&
    $_SERVER['REQUEST_METHOD'] !== "POST")
{
    http_response_code(405);
    return 0;
}

if (isset($_GET['id']) !== true)
{
    http_response_code(400);
    return 0;
}

$id = (int)$_GET['id'];

require_once("./libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    return 1;
}

$sections = Database::Get()->Query("SELECT `".Database::Get()->GetPrefix()."pattern_section`.`id` AS `pattern_section_id`,\n".
                                   "    `".Database::Get()->GetPrefix()."pattern_section`.`id_template_section` AS `pattern_section_id_template_section`,\n".
                                   "    `".Database::Get()->GetPrefix()."template_section`.`name` AS `name`,\n".
                                   "    `".Database::Get()->GetPrefix()."template_section`.`title` AS `title`,\n".
                                   "    `".Database::Get()->GetPrefix()."template_section`.`id_template` AS `id_template`\n".
                                   "FROM `".Database::Get()->GetPrefix()."pattern_section`\n".
                                   "INNER JOIN `".Database::Get()->GetPrefix()."template_section` ON\n".
                                   "    `".Database::Get()->GetPrefix()."pattern_section`.`id_template_section`=\n".
                                   "    `".Database::Get()->GetPrefix()."template_section`.`id`\n".
                                   "WHERE `".Database::Get()->GetPrefix()."pattern_section`.`id_pattern`=?\n".
                                   "ORDER BY `".Database::Get()->GetPrefix()."pattern_section`.`id` ASC\n",
                                   array($id),
                                   array(Database::TYPE_INT));

if (is_array($sections) !== true)
{
    http_response_code(500);
    return 1;
}

$maxSections = count($sections);

if ($maxSections <= 0)
{
    http_response_code(404);
    return 0;
}

if ($_SERVER['REQUEST_METHOD'] === "POST")
{
    if (isset($_GET['id_section']) !== true)
    {
        http_response_code(400);
        return 0;
    }

    if (isset($_POST['text']) !== true)
    {
        http_response_code(400);
        return 0;
    }

    $idSectionTarget = (int)$_GET['id_section'];
    $sectionFound = false;

    for ($i = 0; $i < $maxSections; $i++)
    {
        if (((int)$sections[$i]['pattern_section_id']) === $idSectionTarget)
        {
            $sectionFound = true;
            break;
        }
    }

    if ($sectionFound == false)
    {
        http_response_code(400);
        return 0;
    }

    $idNewRevision = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."pattern_section_revision` (`id`,\n".
                                             "    `text`,\n".
                                             "    `datetime_created`,\n".
                                             "    `id_pattern_section`)\n".
                                             "VALUES (?, ?, UTC_TIMESTAMP(), ?)",
                                             array(NULL, $_POST['text'], $idSectionTarget),
                                             array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_INT));

    if ($idNewRevision <= 0)
    {
        http_response_code(500);
        exit(1);
    }
}

$revisions = Database::Get()->Query("SELECT `".Database::Get()->GetPrefix()."pattern_section`.`id` AS `pattern_section_id`,\n".
                                    "    `".Database::Get()->GetPrefix()."pattern_section`.`id_template_section` AS `pattern_section_id_template_section`,\n".
                                    "    `".Database::Get()->GetPrefix()."pattern_section_revision`.`id` AS `pattern_section_revision_id`,\n".
                                    "    `".Database::Get()->GetPrefix()."pattern_section_revision`.`text` AS `pattern_section_revision_text`,\n".
                                    "    `".Database::Get()->GetPrefix()."pattern_section_revision`.`datetime_created` AS `pattern_section_revision_datetime_created`\n".
                                    "FROM `".Database::Get()->GetPrefix()."pattern_section_revision`\n".
                                    "INNER JOIN `".Database::Get()->GetPrefix()."pattern_section` ON\n".
                                    "    `".Database::Get()->GetPrefix()."pattern_section`.`id` =\n".
                                    "    `".Database::Get()->GetPrefix()."pattern_section_revision`.`id_pattern_section`\n".
                                    "WHERE `".Database::Get()->GetPrefix()."pattern_section`.`id_pattern`=?\n".
                                    "ORDER BY `".Database::Get()->GetPrefix()."pattern_section`.`id` ASC,\n".
                                    "    `".Database::Get()->GetPrefix()."pattern_section_revision`.`datetime_created` DESC\n",
                                    array($id),
                                    array(Database::TYPE_INT));

if (is_array($revisions) !== true)
{
    http_response_code(500);
    return 1;
}

$maxRevisions = count($revisions);

if ($maxRevisions <= 0)
{
    http_response_code(404);
    return 0;
}

$templateId = (int)$sections[0]['id_template'];

$template = Database::Get()->Query("SELECT `id`,\n".
                                   "    `namespace`,\n".
                                   "    `name`,\n".
                                   "    `title`\n".
                                   "FROM `".Database::Get()->GetPrefix()."template`\n".
                                   "WHERE `id`=?\n",
                                   array($templateId),
                                   array(Database::TYPE_INT));

if (is_array($template) !== true)
{
    http_response_code(500);
    return 1;
}

if (count($template) <= 0)
{
    http_response_code(500);
    return 1;
}

$template = $template[0];


require_once("./libraries/negotiation.inc.php");

NegotiateContentType(array(CONTENT_TYPE_SUPPORTED_XHTML,
                           CONTENT_TYPE_SUPPORTED_XML));

if (CONTENT_TYPE_REQUESTED === CONTENT_TYPE_SUPPORTED_XHTML)
{
    // If the name isn't a valid XML tag name and contains special XML characters,
    // escaping them is deliberately supposed to break the format, as such a situation
    // should be prevented on capturing the input.
    $templateName = htmlspecialchars($template['name'], ENT_XHTML, "UTF-8");
    $templateTitle = htmlspecialchars($template['title'], ENT_XHTML, "UTF-8");
    $templateNamespace = htmlspecialchars($template['namespace'], ENT_XHTML | ENT_QUOTES, "UTF-8");

    require_once("./libraries/languagelib.inc.php");
    require_once(getLanguageFile("pattern_view"));

    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
         "<!DOCTYPE html\n".
         "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
         "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
         "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\">\n".
         "  <head>\n".
         "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
         "    <title>".$templateTitle."</title>\n".
         "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
         "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
         "    <style type=\"text/css\">\n".
         "      .revisions\n".
         "      {\n".
         "          display: none;\n".
         "      }\n".
         "    </style>\n".
         "    <script type=\"text/javascript\" src=\"pattern_section_controls.js\"></script>\n".
         "  </head>\n".
         "  <body>\n".
         "    <div class=\"mainbox\">\n".
         "      <div class=\"mainbox_header\">\n".
         "        <h1 class=\"mainbox_header_h1\">".$templateTitle."</h1>\n".
         "      </div>\n".
         "      <div class=\"mainbox_body\">\n".
         "        <div>\n";

    for ($i = 0; $i < $maxSections; $i++)
    {
        echo "          <div>\n";

        if ($templateId != ((int)$sections[$i]['id_template']))
        {
            echo "<!-- Different templates associated with the sections of a pattern! -->\n";
            return 1;
        }

        $firstFound = false;

        /** @todo Optimize: $revisions is already ordered, $j doesn't need to restart from 0. */
        for ($j = 0; $j < $maxRevisions; $j++)
        {
            if (((int)$revisions[$j]['pattern_section_id_template_section']) == ((int)$sections[$i]['pattern_section_id_template_section']))
            {
                $revisionText = htmlspecialchars($revisions[$j]['pattern_section_revision_text'], ENT_XHTML, "UTF-8");

                if ($firstFound == false)
                {
                    $sectionTitle = htmlspecialchars($sections[$i]['title'], ENT_XHTML, "UTF-8");

                    echo "            <h2>".$sectionTitle."</h2>\n".
                         "            <div id=\"update_control_".$sections[$i]['pattern_section_id']."\"></div>\n".
                         "            <p id=\"revision_text_".$sections[$i]['pattern_section_id']."\">".$revisionText."</p>\n".
                         "            <a href=\"#\" onclick=\"LoadInputControl(".$id.", ".$sections[$i]['pattern_section_id'].", true);\">".LANG_LINKCAPTION_EDIT."</a> <a href=\"#\" onclick=\"ToggleRevisions('revisions_".$sections[$i]['pattern_section_id']."');\">".LANG_LINKCAPTION_REVISIONS."</a>\n".
                         "            <div class=\"revisions\" id=\"revisions_".$sections[$i]['pattern_section_id']."\">\n".
                         "              <table border=\"1\">\n".
                         "                <thead>\n".
                         "                  <tr>\n".
                         "                    <th>".LANG_TABLECOLUMNCAPTION_TIMESTAMP." (UTC)</th>\n".
                         "                    <th>".LANG_TABLECOLUMNCAPTION_VERSION."</th>\n".
                         "                  </tr>\n".
                         "                </thead>\n".
                         "                <tbody>\n";

                    $firstFound = true;
                }

                echo "                  <tr>\n".
                     "                    <td>".$revisions[$j]['pattern_section_revision_datetime_created']."Z</td>\n".
                     "                    <td>".$revisionText."</td>\n".
                     "                  </tr>\n";
            }
        }

        if ($firstFound == true)
        {
            echo "                </tbody>\n".
                 "              </table>\n".
                 "            </div>\n";
        }

        echo "          </div>\n";
    }

    echo "        </div>\n".
         "        <div>\n".
         "          (<a href=\"pattern_view.php?id=".$id."&amp;format=xml\">".LANG_LINKCAPTION_VIEWSOURCE."</a>)\n".
         "        </div>\n".
         "        <div>\n".
         "          <a href=\"pattern_select.php?id_template=".$templateId."\">".LANG_LINKCAPTION_PATTERNINDEX."</a>\n".
         "        </div>\n".
         "      </div>\n".
         "    </div>\n".
         "  </body>\n".
         "</html>\n".
         "\n";
}
else if (CONTENT_TYPE_REQUESTED === CONTENT_TYPE_SUPPORTED_XML)
{
    // If the name isn't a valid XML tag name and contains special XML characters,
    // escaping them is deliberately supposed to break the format, as such a situation
    // should be prevented on capturing the input.
    $templateName = htmlspecialchars($template['name'], ENT_XML1, "UTF-8");
    $templateNamespace = htmlspecialchars($template['namespace'], ENT_XML1 | ENT_QUOTES, "UTF-8");

    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
         "<".$templateName." xmlns=\"".$templateNamespace."\">";

    for ($i = 0; $i < $maxSections; $i++)
    {
        if ($templateId != ((int)$sections[$i]['id_template']))
        {
            echo "<!-- Different templates associated with the sections of a pattern! -->\n";
            return 1;
        }

        // If the name isn't a valid XML tag name and contains special XML characters,
        // escaping them is deliberately supposed to break the format, as such a situation
        // should be prevented on capturing the input.
        $sectionName = htmlspecialchars($sections[$i]['name'], ENT_XML1, "UTF-8");

        echo "<".$sectionName.">";

        /** @todo Optimize: $revisions is already ordered, $j doesn't need to restart from 0. */
        for ($j = 0; $j < $maxRevisions; $j++)
        {
            if (((int)$revisions[$j]['pattern_section_id_template_section']) == ((int)$sections[$i]['pattern_section_id_template_section']))
            {
                echo htmlspecialchars($revisions[$j]['pattern_section_revision_text'], ENT_XML1, "UTF-8");
                break;
            }
        }

        echo "</".$sectionName.">";
    }

    echo "</".$templateName.">\n";
}
else
{
    http_response_code(501);
    return 1;
}



?>
