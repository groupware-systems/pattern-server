<?php
/* Copyright (C) 2012-2020  Stephan Kreutzer
 *
 * This file is part of pattern-server.
 *
 * pattern-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * pattern-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with pattern-server. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/index.php
 * @brief Start page.
 * @todo HTTP language negotiation.
 * @author Stephan Kreutzer
 * @since 2020-03-26
 */



require_once("./libraries/https.inc.php");


require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("index"));
require_once("./language_selector.inc.php");

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>".LANG_PAGETITLE."</title>\n".
     "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "  </head>\n".
     "  <body>\n";

require_once("./language_selector.inc.php");
echo getHTMLLanguageSelector("index.php");

echo "    <div class=\"mainbox\">\n".
     "      <div class=\"mainbox_header\">\n".
     "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
     "      </div>\n".
     "      <div class=\"mainbox_body\">\n";

if (isset($_POST['install_done']) == true)
{
    if (@unlink(dirname(__FILE__)."/install/install.php") === true)
    {
        clearstatcache();
    }
    else
    {
        echo "        <p class=\"error\">\n".
             "          ".LANG_INSTALLDELETEFAILED."\n".
             "        </p>\n";
    }
}

if (file_exists("./install/install.php") === true &&
    isset($_GET['skipinstall']) != true)
{
    echo "        <form action=\"install/install.php\" method=\"post\" class=\"installbutton_form\">\n".
         "          <fieldset>\n".
         "            <input type=\"submit\" value=\"".LANG_INSTALLBUTTON."\"/><br/>\n".
         "          </fieldset>\n".
         "        </form>\n";

    require_once("./license.inc.php");
    echo getHTMLLicenseNotification("license");
}
else
{
    echo "        <p>\n".
         "          ".LANG_WELCOMETEXT."\n".
         "        </p>\n".
         "        <ul>\n".
         "          <li>\n".
         "            <a href=\"template_create.php\">".LANG_LINKCAPTION_CREATETEMPLATE."</a>\n".
         "          </li>\n".
         "          <li>\n".
         "            <a href=\"template_select.php?target=2\">".LANG_LINKCAPTION_CREATEPATTERN."</a>\n".
         "          </li>\n".
         "          <li>\n".
         "            <a href=\"template_select.php?target=1\">".LANG_LINKCAPTION_BROWSEPATTERNS."</a>\n".
         "          </li>\n".
         "        </ul>\n";

    require_once("./license.inc.php");
    echo getHTMLLicenseNotification("license");
}

echo "      </div>\n".
     "    </div>\n".
     "    <div class=\"footerbox\">\n".
     "      <a href=\"license.php\" class=\"footerbox_link\">".LANG_LICENSE."</a>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n".
     "\n";


?>
