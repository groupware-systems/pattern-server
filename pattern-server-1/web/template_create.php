<?php
/* Copyright (C) 2020-2024 Stephan Kreutzer
 *
 * This file is part of pattern-server.
 *
 * pattern-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * pattern-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with pattern-server. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/template_create.php
 * @brief For creating a new pattern template.
 * @author Stephan Kreutzer
 * @since 2020-08-27
 */



require_once("./libraries/https.inc.php");


require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("template_create"));



function validateXmlElementName($name)
{
    if (is_string($name) !== true)
    {
        return false;
    }

    if (strlen($name) <= 0)
    {
        return false;
    }

    /** @todo This might depend on locale. */
    if (ctype_alpha($name[0]) !== true &&
        $name[0] != '_')
    {
        return false;
    }

    if (stripos($name, "xml") === 0)
    {
        return false;
    }

    for ($i = 1, $max = strlen($name); $i < $max; $i++)
    {
        /** @todo This might depend on locale. */
        if (ctype_alnum($name[$i]) !== true &&
            $name[$i] != '-' &&
            $name[$i] != '_' &&
            $name[$i] != '.')
        {
            return false;
        }
    }

    return true;
}



$sections = array();

if (count($_POST) > 0)
{
    /** @todo There might be no guarantee of the order of the input elements
      * being retained in $_POST, but that would be important for the patterns. */

    foreach ($_POST as $key => $value)
    {
        if (strpos($key, "section-name-") === 0)
        {
            $id = substr($key, strlen("section-name-"));

            if ($id === false)
            {
                http_response_code(500);
                return 1;
            }

            if (strlen($id) <= 0)
            {
                http_response_code(400);
                return 1;
            }

            if (isset($_POST["section-title-".$id]) !== true)
            {
                http_response_code(400);
                return 1;
            }

            if (isset($sections[$id]) === true)
            {
                http_response_code(400);
                return 1;
            }

            if (validateXmlElementName($_POST["section-name-".$id]) != true)
            {
                http_response_code(400);
                return 1;
            }

            $sections[$id] = array("name" => $_POST["section-name-".$id],
                                   "title" => $_POST["section-title-".$id]);
        }
    }
}

$inserted = false;

if (isset($_POST['name']) === true &&
    isset($_POST['title']) === true &&
    isset($_POST['namespace']) === true &&
    count($sections) > 0)
{
    if (validateXmlElementName($_POST["name"]) != true)
    {
        http_response_code(400);
        return 1;
    }

    require_once("./libraries/database.inc.php");

    if (Database::Get()->IsConnected() !== true)
    {
        http_response_code(500);
        return 1;
    }

    $idTemplate = -1;
    $success = Database::Get()->BeginTransaction();

    if ($success === true)
    {
        $idTemplate = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."template` (`id`,\n".
                                              "    `namespace`,\n".
                                              "    `name`,\n".
                                              "    `title`,\n".
                                              "    `datetime_created`)\n".
                                              "VALUES (?, ?, ?, ?, UTC_TIMESTAMP())\n",
                                              array(NULL, $_POST['namespace'], $_POST['name'], $_POST['title']),
                                              array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_STRING, Database::TYPE_STRING));

        if ($idTemplate <= 0)
        {
            $success = false;
        }
    }

    /** @todo Check if it maybe would be more performant to add all values
      * to a single INSERT, but the downside is preparing the PDO arrays. */

    foreach ($sections as $section)
    {
        $idSection = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."template_section` (`id`,\n".
                                              "    `name`,\n".
                                              "    `title`,\n".
                                              "    `id_template`)\n".
                                              "VALUES (?, ?, ?, ?)\n",
                                              array(NULL, $section['name'], $section['title'], $idTemplate),
                                              array(Database::TYPE_NULL, Database::TYPE_STRING, Database::TYPE_STRING, Database::TYPE_INT));

        if ($idSection <= 0)
        {
            $success = false;
            break;
        }
    }

    if ($success === true)
    {
        if (Database::Get()->CommitTransaction() === true)
        {
            $inserted = true;
        }
        else
        {
            http_response_code(500);
            return 1;
        }
    }
    else
    {
        Database::Get()->RollbackTransaction();
        http_response_code(500);
        return 1;
    }
}


if ($inserted !== true)
{
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
         "<!DOCTYPE html\n".
         "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
         "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
         "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
         "  <head>\n".
         "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
         "    <title>".LANG_PAGETITLE."</title>\n".
         "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
         "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
         "    <style type=\"text/css\">\n".
         "      .container-section\n".
         "      {\n".
         "          border: 1px solid black;\n".
         "      }\n".
         "\n".
         "      .validation:invalid\n".
         "      {\n".
         "          border: #FF0000 solid 3px;\n".
         "      }\n".
         "    </style>\n".
         "    <script type=\"text/javascript\">\n".
         "      \"use strict\";\n".
         "\n".
         "      let sectionCount = 0;\n".
         "\n".
         "      function addSection()\n".
         "      {\n".
         "          let target = document.getElementById(\"sections\");\n".
         "\n".
         "          if (target == null)\n".
         "          {\n".
         "              console.log(\"Was unable to find element with the ID \\\"sections\\\".\");\n".
         "              return -1;\n".
         "          }\n".
         "\n".
         "          let container = document.createElement(\"div\");\n".
         "          container.setAttribute(\"id\", \"section-\" + sectionCount);\n".
         "          container.setAttribute(\"class\", \"container-section\");\n".
         "\n".
         "          let label = document.createElement(\"label\");\n".
         "          label.setAttribute(\"for\", \"section-title-\" + sectionCount);\n".
         "          label.appendChild(document.createTextNode(\"".LANG_FORMLABEL_PATTERNSECTIONTITLE."\"));\n".
         "          container.appendChild(label);\n".
         "          let title = document.createElement(\"input\");\n".
         "          title.setAttribute(\"type\", \"text\");\n".
         "          title.setAttribute(\"name\", \"section-title-\" + sectionCount);\n".
         "          title.setAttribute(\"id\", \"section-title-\" + sectionCount);\n".
         "          title.setAttribute(\"required\", \"true\");\n".
         "          container.appendChild(title);\n".
         "\n".
         "          label = document.createElement(\"label\");\n".
         "          label.setAttribute(\"for\", \"section-name-\" + sectionCount);\n".
         "          label.appendChild(document.createTextNode(\"".LANG_FORMLABEL_PATTERNSECTIONNAME."\"));\n".
         "          container.appendChild(label);\n".
         "          let name = document.createElement(\"input\");\n".
         "          name.setAttribute(\"type\", \"text\");\n".
         "          name.setAttribute(\"name\", \"section-name-\" + sectionCount);\n".
         "          name.setAttribute(\"id\", \"section-name-\" + sectionCount);\n".
         // Pattern isn't perfect, but useful enough. The server may still refuse it.
         "          name.setAttribute(\"pattern\", \"[a-zA-Z]{1}[a-zA-Z0-9\\\\-]{0,254}\");\n".
         "          name.setAttribute(\"required\", \"true\");\n".
         "          name.classList.add(\"validation\");\n".
         "          container.appendChild(name);\n".
         "\n".
         "          let remove = document.createElement(\"input\");\n".
         "          remove.setAttribute(\"type\", \"button\");\n".
         "          remove.setAttribute(\"value\", \"-\");\n".
         "          remove.setAttribute(\"onclick\", \"removeSection(\\\"section-\" + sectionCount + \"\\\");\");\n".
         "          container.appendChild(remove);\n".
         "\n".
         "          target.appendChild(container);\n".
         "          sectionCount += 1;\n".
         "\n".
         "          return 0;\n".
         "      }\n".
         "\n".
         "      function removeSection(id)\n".
         "      {\n".
         "          let target = document.getElementById(id);\n".
         "\n".
         "          if (target == null)\n".
         "          {\n".
         "              return 1;\n".
         "          }\n".
         "\n".
         "          for (let i = target.childNodes.length - 1; i >= 0; i--)\n".
         "          {\n".
         "              removeNode(target.childNodes[i], target);\n".
         "          }\n".
         "\n".
         "          // Doesn't matter that the empty div container remains here,\n".
         "          // is of no effect for the form.\n".
         "\n".
         "          return 0;\n".
         "      }\n".
         "\n".
         "      // Stupid JavaScript has a cloneNode(deep), but no removeNode(deep).\n".
         "      function removeNode(element, parent)\n".
         "      {\n".
         "          // TODO: Reverse delete for performance.\n".
         "          while (element.hasChildNodes() == true)\n".
         "          {\n".
         "              removeNode(element.lastChild, element);\n".
         "          }\n".
         "\n".
         "          parent.removeChild(element);\n".
         "      }\n".
         "\n".
         "      window.onload = function() {\n".
         "          addSection();\n".
         "      };\n".
         "    </script>\n".
         "  </head>\n".
         "  <body>\n".
         "    <div class=\"mainbox\">\n".
         "      <div class=\"mainbox_header\">\n".
         "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
         "      </div>\n".
         "      <div class=\"mainbox_body\">\n".
         "        <form method=\"post\" action=\"template_create.php\">\n".
         "          <fieldset>\n".
         "            <label for=\"title\">".LANG_FORMLABEL_PATTERNTITLE."</label>\n".
         "            <input type=\"text\" name=\"title\" id=\"title\" required=\"true\"/>\n".
         "            <label for=\"name\">".LANG_FORMLABEL_PATTERNNAME."</label>\n".
         // Pattern isn't perfect, but useful enough. The server may still refuse it.
         "            <input type=\"text\" name=\"name\" id=\"name\" pattern=\"[a-zA-Z]{1}[a-zA-Z0-9\\-]{0,254}\" required=\"true\" class=\"validation\"/>\n".
         "            <label for=\"namespace\">".LANG_FORMLABEL_PATTERNNAMESPACE."</label>\n".
         "            <input type=\"text\" name=\"namespace\" id=\"namespace\" required=\"true\"/>\n".
         "            <div id=\"sections\"></div>\n".
         "            <input type=\"button\" value=\"+\" onclick=\"addSection();\"/>\n".
         "            <input type=\"submit\" value=\"".LANG_FORMLABEL_SUBMIT."\"/>\n".
         "          </fieldset>\n".
         "        </form>\n".
         "        <div>\n".
         "          <a href=\"index.php\">".LANG_LINKCAPTION_MAINPAGE."</a>\n".
         "        </div>\n".
         "      </div>\n".
         "    </div>\n".
         "  </body>\n".
         "</html>\n".
         "\n";
}
else
{
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
         "<!DOCTYPE html\n".
         "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
         "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
         "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
         "  <head>\n".
         "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
         "    <title>".LANG_PAGETITLE."</title>\n".
         "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
         "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
         "  </head>\n".
         "  <body>\n".
         "    <div class=\"mainbox\">\n".
         "      <div class=\"mainbox_header\">\n".
         "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
         "      </div>\n".
         "      <div class=\"mainbox_body\">\n".
         "        <a href=\"index.php\">".LANG_LINKCAPTION_MAINPAGE."</a>\n".
         "      </div>\n".
         "    </div>\n".
         "  </body>\n".
         "</html>\n".
         "\n";
}


?>
