/* Copyright (C) 2019-2023 Stephan Kreutzer
 *
 * This file is part of pattern-server.
 *
 * pattern-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * pattern-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with pattern-server. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function LoadInputControl(idPattern, idSection, show)
{
    let updateControlDiv = document.getElementById("update_control_" + idSection);
    let revisionTextSpan = document.getElementById("revision_text_" + idSection);

    if (updateControlDiv == null)
    {
        throw "ID 'update_control_" + idSection + "' not found.";
    }

    if (revisionTextSpan == null)
    {
        throw "ID 'revision_text_" + idSection + "' not found.";
    }

    for (let i = updateControlDiv.childNodes.length - 1; i >= 0; i--)
    {
        removeNode(updateControlDiv.childNodes[i], updateControlDiv);
    }

    if (show == true)
    {
        let form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "pattern_view.php?id=" + idPattern + "&id_section=" + idSection);

        let fieldset = document.createElement("fieldset");

        let textarea = document.createElement("textarea");
        textarea.setAttribute("name", "text");
        textarea.setAttribute("rows", "24");
        textarea.setAttribute("cols", "80");

        {
            let textareaText = document.createTextNode(revisionTextSpan.innerText);
            textarea.appendChild(textareaText);
        }

        let submit = document.createElement("input");
        submit.setAttribute("type", "submit");
        submit.setAttribute("name", "submit");
        submit.setAttribute("value", "Update");

        let cancel = document.createElement("input");
        cancel.setAttribute("type", "button");
        cancel.setAttribute("onclick", "LoadInputControl(" + idPattern + ", " + idSection + ", false);");
        cancel.setAttribute("value", "Cancel");

        fieldset.appendChild(textarea);
        fieldset.appendChild(submit);
        fieldset.appendChild(cancel);

        form.appendChild(fieldset);
        updateControlDiv.appendChild(form);
    }

    return 0;
}

function ToggleRevisions(id)
{
    let revisionsDiv = document.getElementById(id);

    if (revisionsDiv == null)
    {
        return -1;
    }

    if (revisionsDiv.style.display === "block")
    {
        revisionsDiv.style.display = "none";
    }
    else
    {
        revisionsDiv.style.display = "block";
    }

    return 0;
}

// Stupid JavaScript has a cloneNode(deep), but no removeNode(deep).
function removeNode(element, parent)
{
    // TODO: Reverse delete for performance.
    while (element.hasChildNodes() == true)
    {
        removeNode(element.lastChild, element);
    }

    parent.removeChild(element);
}
