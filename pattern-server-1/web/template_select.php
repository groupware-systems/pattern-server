<?php
/* Copyright (C) 2020-2023 Stephan Kreutzer
 *
 * This file is part of pattern-server.
 *
 * pattern-server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * pattern-server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with pattern-server. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/template_select.php
 * @brief For selecting a pattern template.
 * @author Stephan Kreutzer
 * @since 2020-08-27
 */



require_once("./libraries/https.inc.php");


require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("template_select"));

$target = 1;

if (isset($_GET['target']) === true)
{
    $target = (int)$_GET['target'];

    if ($target !== 1 &&
        $target !== 2)
    {
        $target = 1;
    }
}

require_once("./libraries/database.inc.php");

if (Database::Get()->IsConnected() !== true)
{
    http_response_code(500);
    return 1;
}

$templates = Database::Get()->QueryUnsecure("SELECT `id`,\n".
                                            "    `title`\n".
                                            "FROM `".Database::Get()->GetPrefix()."template`\n".
                                            "WHERE 1");

if (is_array($templates) !== true)
{
    http_response_code(500);
    return 1;
}

require_once("./libraries/negotiation.inc.php");

NegotiateContentType(array(CONTENT_TYPE_SUPPORTED_XHTML,
                           CONTENT_TYPE_SUPPORTED_XML,
                           CONTENT_TYPE_SUPPORTED_NCX));


if (CONTENT_TYPE_REQUESTED === CONTENT_TYPE_SUPPORTED_XHTML)
{
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
         "<!DOCTYPE html\n".
         "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
         "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
         "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
         "  <head>\n".
         "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
         "    <title>".LANG_PAGETITLE."</title>\n".
         "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
         "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
         "  </head>\n".
         "  <body>\n".
         "    <div class=\"mainbox\">\n".
         "      <div class=\"mainbox_header\">\n".
         "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
         "      </div>\n".
         "      <div class=\"mainbox_body\">\n".
         "        <ul>\n";

    for ($i = 0, $max = count($templates); $i < $max; $i++)
    {
        echo "          <li>\n";

        if ($target === 2)
        {
            echo "            <a href=\"pattern_create.php?id_template=".htmlspecialchars($templates[$i]['id'], ENT_XHTML | ENT_QUOTES, "UTF-8")."\">".htmlspecialchars($templates[$i]['title'], ENT_XHTML, "UTF-8")."</a>\n";
        }
        else
        {
            echo "            <a href=\"pattern_select.php?id_template=".htmlspecialchars($templates[$i]['id'], ENT_XHTML | ENT_QUOTES, "UTF-8")."\">".htmlspecialchars($templates[$i]['title'], ENT_XHTML, "UTF-8")."</a>\n";
        }

        echo "          </li>\n";
    }

    echo "        </ul>\n".
         "        <div>\n".
         "          (<a href=\"template_select.php?target=".$target."&amp;format=ncx\">".LANG_LINKCAPTION_VIEWSOURCE."</a>)\n".
         "        </div>\n".
         "        <div>\n".
         "          <a href=\"index.php\">".LANG_LINKCAPTION_MAINPAGE."</a>\n".
         "        </div>\n".
         "      </div>\n".
         "    </div>\n".
         "  </body>\n".
         "</html>\n";
}
else if (CONTENT_TYPE_REQUESTED === CONTENT_TYPE_SUPPORTED_XML ||
         CONTENT_TYPE_REQUESTED === CONTENT_TYPE_SUPPORTED_NCX)
{
    $protocol = "https://";

    if (HTTPS_ENABLED !== true)
    {
        $protocol = "http://";
    }

    $url = $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
         "<ncx:ncx xmlns:ncx=\"http://www.daisy.org/z3986/2005/ncx/\" version=\"2005-1\" xml:lang=\"".getCurrentLanguage()."\">\n".
         "  <ncx:head>\n".
         "    <ncx:meta name=\"dc:Title\" content=\"".LANG_NCX_TITLE."\"/>\n".
         /** @todo Should be an abstract URI, not an URL. */
         "    <ncx:meta name=\"dtb:uid\" content=\"".htmlspecialchars($url, ENT_XML1 | ENT_QUOTES, "UTF-8")."\"/>\n".
         "  </ncx:head>\n".
         "  <ncx:docTitle>\n".
         "    <ncx:text>".LANG_NCX_TITLE."</ncx:text>\n".
         "  </ncx:docTitle>\n".
         "  <ncx:navMap>\n";

    for ($i = 0, $max = count($templates); $i < $max; $i++)
    {
        echo "    <ncx:navPoint id=\"id_resource_".($i + 1)."\" playOrder=\"".($i + 1)."\">\n".
             "      <ncx:navLabel>\n".
             "        <ncx:text>".htmlspecialchars($templates[$i]['title'], ENT_XML1, "UTF-8")."</ncx:text>\n".
             "      </ncx:navLabel>\n";

        if ($target === 2)
        {
            echo "      <ncx:content src=\"pattern_create.php?id_template=".htmlspecialchars($templates[$i]['id'], ENT_XML1 | ENT_QUOTES, "UTF-8")."\"/>\n";
        }
        else
        {
            echo "      <ncx:content src=\"pattern_select.php?id_template=".htmlspecialchars($templates[$i]['id'], ENT_XML1 | ENT_QUOTES, "UTF-8")."\"/>\n";
        }

        echo "    </ncx:navPoint>\n";
    }

    echo "  </ncx:navMap>\n".
         "</ncx:ncx>\n";
}
else
{
    http_response_code(501);
    return 1;
}



?>
